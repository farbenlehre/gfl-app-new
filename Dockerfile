FROM php:8.1-rc-fpm-alpine3.17

ENV COMPOSER_ALLOW_SUPERUSER 1
ENV APP_ENV prod

ENV COMPOSER_ALLOW_SUPERUSER=1 \
    DEV_DEPS="libzip-dev bzip2-dev openjpeg-dev imagemagick-dev libjpeg-turbo-dev libpng-dev libxslt-dev icu-dev" \
    BUILD_TOOLS="autoconf automake g++ libtool gcc yarn git nodejs-current"

WORKDIR /var/www/html/
COPY . /var/www/html/

RUN apk add --update --no-cache \
        file \
        build-base \
        nasm \
        musl \
        libjpeg-turbo \
        libjpeg-turbo-utils \
        npm \
        python3 \
        imagemagick \
        wget \
        libzip \
        $DEV_DEPS \
        $BUILD_TOOLS && \
    docker-php-ext-configure gd --with-jpeg && \
    docker-php-ext-configure intl && \
    # Install PHP extensions
    docker-php-ext-install \
        gd \
        intl \
        opcache && \
    pecl install \
        imagick \
        apcu && \
    docker-php-ext-enable \
        imagick \
        apcu && \
    printf "memory_limit=1024M" > /usr/local/etc/php/conf.d/memory-limit.ini && \
    printf '[PHP]\ndate.timezone = "Europe/Berlin"\n' > /usr/local/etc/php/conf.d/tzone.ini && \
    docker-php-source delete && \
    curl -sS https://getcomposer.org/installer | php && \
    npm i -g corepack && \
    php composer.phar install --prefer-dist --no-progress --optimize-autoloader --classmap-authoritative  --no-interaction && \
    php composer.phar clearcache && \
    mkdir -p /var/www/html/public/build && \
    chmod -R 777 /var/www/html/public/build && \
    # Compile frontend assets
    npm ci && \
    npx encore production && \
    # Clean up
    rm -rf node_modules && \
    apk del --purge \
        $BUILD_TOOLS \
        $DEV_DEPS && \
    rm -rf /var/cache/apk/* && \
    chmod -R 777 /var/www/html/var

VOLUME /var/www/html/
