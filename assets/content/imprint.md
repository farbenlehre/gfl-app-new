<h1>Impressum</h1>
<br>
Bitte lesen Sie die Hinweise zum <a href="https://www.sub.uni-goettingen.de/impressum" target="_blank">Impressum</a> auf der Seite der Niedersächsischen Staats- und Universitätsbibliothek Göttingen

Den Hinweis zur technischen Umsetzung finden Sie unter <a href="/kontakt#technische_umsetzung">Kontakt<a>.


