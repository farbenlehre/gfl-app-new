import Search from "./modules/search";

import 'bootstrap/js/src/dropdown';

require('../scss/app.scss');
require('../images/logo.png')
require('../images/logo_dfg.svg');
require('../images/logo_fnsnf.svg');
require('../images/logo_sub.svg');
require('../images/logo_uni_basel.svg');
require('../images/logo_tu_berlin.svg');
require('../icons/apple-touch-icon.png');
require('../icons/android-chrome-192x192.png');
require('../icons/android-chrome-512x512.png');
require('../icons/favicon-32x32.png');
require('../icons/favicon-16x16.png');

document.querySelector('.navbar-toggler').addEventListener('click', () => {
    const menu = document.querySelector('.navbar-collapse');
    menu.classList.toggle('collapse');
});

(() => {
    if (document.querySelector('.search-form')) {
        new Search();
    }
})();
