class Search {
  constructor() {
    this.lastFieldGroupIndex = -1;
    this.container = document.querySelector('.search-form .fields');
    this.submitButton = document.querySelector('.search-form button[type="submit"]');
    this.dateMin = 1810;
    this.dateMax = 1832;
    this.createAddFieldGroupButton();

    const url = new URL(window.location);
    const query = url.searchParams;
    const entries = Object.fromEntries(query.entries());


    let queryArr = [];
    Object.keys(entries).forEach(key => {
      let index = key.match(/\d+/g)?.[0];
      let attribute = key.match(/field|operator|value|phrase/g)?.[0];
      const value = entries[key];

      if (index === undefined && !attribute) {
        return;
      }

      index = parseInt(index);

      if (queryArr.length <= index) {
        queryArr.push({
          operator: '',
          field: '',
          value: '',
          phrase: ''
        })
      }

      if (attribute) {
        queryArr[index][attribute] = value;
      }
    });

    const queryValid = this.validateQuery(queryArr);

    if (!queryValid || queryArr.length === 0) {
      // TODO: send message

      queryArr = [{
        operator: '',
        field: '',
        value: '*',
        phrase: ''
      }];
    }

    queryArr.forEach((queryItem, index) => {
      this.lastFieldGroupIndex = index;
      this.addFieldGroup(this.getFieldGroupData(index, queryItem));
    });

    // Setup sort
    const sortSelect = document.getElementById('sort');

    if (sortSelect) {
      sortSelect.addEventListener('change', () => {
        const value = sortSelect.value;
        if (!value) {
          return;
        }

        const [sorting, direction] = value.split('-');

        url.searchParams.set('sorting', sorting);
        url.searchParams.set('direction', direction);

        window.history.pushState({ path: url.href }, '', url.href);
        window.location.reload();
      })
    }

    // Easteregg
    this.submitButton.addEventListener('click',  (e) => {
      const inputs = document.querySelectorAll('input[type="text"].form-control');

      if (inputs.length === 3) {
        const filtered = [...inputs].filter(input => input.value.toLowerCase() !== 'wein');
        if (filtered.length === 0) {
          e.preventDefault();
          const style = document.createElement('style');
          style.innerHTML = `
            body * {
              text-shadow: .035em .04em 0 rgba(28, 25, 169, 0.35);
              animation: drunken 6s infinite;
            }
            
            @keyframes drunken {
              0% {
                text-shadow: .035em .04em 0 rgba(28, 25, 169, 0.35);
              }
              25% {
                text-shadow: .63em -.05em 0 rgba(169, 25, 25, 0.35);
              }
              75% {
                text-shadow: -.63em -.05em 0 rgba(169, 25, 25, 0.35);
              }
              100% {
                text-shadow: .035em .04em 0 rgba(28, 25, 169, 0.35);
              }
            }`;
          document.head.appendChild(style);
        }
      }
    });
  }

  validateQuery(queryArr) {
    let i = 0;
    let validated = false;
    if (queryArr.length > 0) {
      do {
        validated = this.validateQueryGroup(queryArr[i]);
        i++;
      } while (validated && i < queryArr.length);
    }

    return validated;
  }

  validateQueryGroup(queryGroup) {
    const keys = ['operator', 'field', 'value'];
    let validated = false;
    let i = 0;

    do {
      validated = queryGroup.hasOwnProperty(keys[i]) && queryGroup[keys[i]] !== '';
      i++;
    } while (validated && i < keys.length)

    return validated;
  }

  createAddFieldGroupButton() {
    const button = document.querySelector('.search-form .add-field-group-button');
    if (button) {
      button.addEventListener('click', (e) => {
        e.preventDefault();
        this.addFieldGroup(this.getFieldGroupData(++this.lastFieldGroupIndex))
      });
    }
  }

  addFieldGroup(fieldsData = []) {
    const fields = fieldsData.map(data => {
      const { type } = data;
      if (type === 'text') {
        return this.getTextField(data);
      } else if (type === 'number') {
        return this.getNumberField(data);
      } else if (type === 'select') {
        return this.getSelectField(data);
      } else if (type === 'switch') {
        return this.getSwitchField(data);
      } else if (type === 'remove') {
        return this.getRemoveButton(data);
      }
    }).join('');

    this.appendTemplate(
      `<div class="search-field-group input-group mb-2">${fields}</div>`
    );

    const fieldGroup = document.querySelector('.search-field-group:last-child');
    const index = [...fieldGroup.parentElement.children].indexOf(fieldGroup);

    fieldGroup
      .querySelector('.field-select')
      .addEventListener('change', (e) => {
        const field = e.target.value;

        let newField = this.getTextField({
          type: 'text',
          name: `q[${index}][value]`,
          value: ''
        });
        if (field === 'id') {
          newField = this.getTextField({
            type: 'text',
            name: `q[${index}][value]`,
            placeholder: 'Die IDs enthalten das Datum in der Form JJJJ-MM-TT.',
            value: ''
          });
        }
        e.target.parentElement.parentElement.querySelector('.flex-column:nth-child(3)')
          .replaceWith(this.convertToElement(newField));
      })

    // this.lastFieldGroupIndex++;
  }

  getRemoveButton({disabled}) {
    return `<button type="button" class="remove-button btn btn-outline-primary ms-2 mt-auto ${disabled ? 'disabled invisible' : ''}" aria-label="Close"
        onclick="this.parentElement.parentElement.removeChild(this.parentElement)">Entfernen
        </button>`;
  }

  getTextField({name, value, placeholder = 'Suchbegriff(e) inkl. Wildcard-Parameter * und ?'}) {
    return `<div class="d-flex flex-column flex-grow-1">
        <label class="small fw-bold">Wert</label>
        <input class="form-control" type="text" name="${name}" value="${value}" placeholder="${placeholder}">
      </div>`;
  }

  getNumberField({name, value, placeholder = `Jahreszahl zwischen ${this.dateMin} und ${this.dateMax}`, min, max}) {
    return `<div class="d-flex flex-column flex-grow-1">
        <label class="small fw-bold">Wert</label>
        <input class="form-control" type="number" min="${min}" max="${max}" name="${name}" value="${value}" placeholder="${placeholder}">
      </div>`;
  }

  getSelectField({name, options, label, classes = '' }) {
    const optionsTemplate = options.map(({value, label, selected, disabled}) =>
      `<option value="${value}" ${selected ? 'selected' : ''} ${disabled ? 'disabled' : ''}>${label}</option>`
    ).join('');

    return `<div class="d-flex flex-column flex-grow-0 flex-shrink-0 pe-2 ${classes}">
        <label class="small fw-bold">${label}</label>
        <select class="form-select w-auto" name="${name}">${optionsTemplate}</select>
      </div>`;
  }

  getSwitchField({name, label, checked}) {
    return `<div class="d-flex flex-column flex-grow-0 flex-shrink-0 ms-2 form-switch">
            <label class="small fw-bold">${label}</label>
            <input class="form-check-input" role="switch" type="checkbox" ${checked ? 'checked' : ''} name="${name}">
        </div>`
  }

  convertToElement(template) {
    const wrapper = document.createElement('div');
    wrapper.innerHTML = template;
    return wrapper.firstChild;
  }

  appendTemplate(template, container = this.container) {
    container.appendChild(this.convertToElement(template));
  }

  insertAfter(newNode, existingNode) {
    existingNode.parentNode.insertBefore(newNode, existingNode.nextSibling);
  }

  getFieldGroupData(index, query = {
    operator: '',
    field: '',
    value: '',
    phrase: ''
  }) {
    const operatorQuery = query['operator'];

    const options = index > 0 ?
      [
        {label: 'AND', value: 'AND', selected: operatorQuery === 'AND'},
        {label: 'OR', value: 'OR', selected: operatorQuery === 'OR'},
        {label: 'NOT', value: 'NOT', selected: operatorQuery === 'NOT'}
      ] :
      [
        {label: 'Kein', value: 'none', selected: operatorQuery === ''},
        {label: 'NOT', value: 'NOT', selected: operatorQuery === 'NOT'}
      ];

    const operatorData = {
      type: 'select',
      name: `q[${index}][operator]`,
      label: 'Operator',
      classes: 'operator-select',
      options
    };

    const fieldQuery = query['field'];
    const fieldData = {
      type: 'select',
      name: `q[${index}][field]`,
      label: 'Feldname',
      classes: 'field-select',
      options: [
        {label: 'Titel', value: 'short_title', selected: fieldQuery === 'short_title'},
        {label: 'Autor/Absender', value: 'author', selected: fieldQuery === 'author'},
        {label: 'Empfänger', value: 'recipient', selected: fieldQuery === 'recipient'},
        {label: 'Entstehungsort', value: 'origin_place', selected: fieldQuery === 'origin_place'},
        {label: 'Bestimmungsort', value: 'destination_place', selected: fieldQuery === 'destination_place'},
        {label: 'Schlagwort', value: 'keywords', selected: fieldQuery === 'keywords'},
        {label: 'Text', value: 'fulltext', selected: fieldQuery === 'fulltext'},
        {label: 'Kommentar', value: 'all_notes', selected: fieldQuery === 'all_notes'},
        {label: 'ID/Datum', value: 'id', selected: fieldQuery === 'id'},
      ]
    };

    const valueQuery = query['value'];
    const valueData =
      (fieldQuery === 'id')
      ? {
        type: 'text',
        name: `q[${index}][value]`,
        placeholder: 'Die IDs enthalten das Datum in der Form JJJJ-MM-TT.',
        value: valueQuery
      } : {
      type: 'text',
      name: `q[${index}][value]`,
      value: valueQuery
    };

    const phraseQuery = query['phrase'];
    const wholeWordData = {
      type: 'switch',
      name: `q[${index}][phrase]`,
      label: 'Ganzes Wort',
      checked: phraseQuery === 'on'
    }

    const removeButtonData = {
      type: 'remove',
      disabled: index === 0,
    };

    return [
      operatorData,
      fieldData,
      valueData,
      wholeWordData,
      removeButtonData
    ];
  }
}

export default Search;
