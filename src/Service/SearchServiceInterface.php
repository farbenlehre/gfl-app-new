<?php

namespace App\Service;


interface SearchServiceInterface
{
    public function getDateFromDocument(array $originDate): array;

    public function getDateFromFilter(array $originDate): array;

    public function getDocumentsFromEntities(string $gndId): array;

    public function getDocumentsFromLiterature(string $litId): array;

    public function getEntities(): array;

    public function getEntitiesById(string $gndId): array;

    public function getFacets(array $facetsArr): array;

    public function getInfo(): array;

    public function getLiterature(): array;

    public function search(array $query, ?string $sort, ?string $order, ?string $searchNotes): array;
}
