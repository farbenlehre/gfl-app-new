<?php

namespace App\Service;

use App\Model\GflArticle;
use App\Model\GflPage;
use Solarium\Client;
use Subugoe\TextApiBundle\View\Annotation\Body;
use Subugoe\TextApiBundle\View\Annotation\Selector;
use Subugoe\TextApiBundle\View\Annotation\Target;
use Subugoe\TextApiBundle\Model\ArticleInterface;
use Subugoe\TextApiBundle\Model\PageInterface;
use Subugoe\TextApiBundle\View\Annotation\Item as AnnotationItem;
use Symfony\Component\Asset\Packages;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class GflTranslator implements \Subugoe\TextApiBundle\Translator\TranslatorInterface
{
    private Client $client;
    private TranslatorInterface $i18n;
    private RouterInterface $router;
    private Packages $packages;
    private string $mainDomain;

    private GflArticle $article;

    public function __construct(Client $client, TranslatorInterface $i18n, RouterInterface $router, Packages $packages, ParameterBagInterface $params)
    {
        $this->client = $client;
        $this->i18n = $i18n;
        $this->router = $router;
        $this->packages = $packages;
        $this->mainDomain = $params->get('main_domain');
    }

    public function setArticle(GflArticle $article): void
    {
        $this->article = $article;
    }

    public function getPageById(string $id): ?PageInterface
    {
        $solrDoc = $this->getDocument($id);

        $page = new GflPage();
        $page->setId($solrDoc['id']);
        $page->setPageNumber($solrDoc['page_number']);
        $page->setArticleId($solrDoc['article_id']);
        $page->setArticleTitle($solrDoc['article_title']);
        $page->setTranscriptedText($solrDoc['transcripted_text']);
        $page->setEditedText($solrDoc['edited_text']);
        $page->setImageUrl($this->router->getContext()->getBaseUrl() . $solrDoc['image_url']);
        $page->setImageLicense($solrDoc['image_license']);
        $page->setImageLicenseUrl($solrDoc['image_license_url']);
        $page->setEnumeration($page->getPageNumber() . ' : ' . $solrDoc['page_foliant']);
        $page->setTitle('Seite ' . $page->getEnumeration());

        if (isset($solrDoc['page_segs']) && !empty($solrDoc['page_segs'])) {
            $page->setPageSegs($solrDoc['page_segs']);
        }

        if (isset($solrDoc['page_notes']) && !empty($solrDoc['page_notes'])) {
            $page->setPageNotes($solrDoc['page_notes']);
        }

        if (isset($solrDoc['page_notes_ids']) && !empty($solrDoc['page_notes_ids'])) {
            $page->setPageNotesIds($solrDoc['page_notes_ids']);
        }

        if (isset($solrDoc['page_notes_abstracts']) && !empty($solrDoc['page_notes_abstracts'])) {
            $page->setPageNotesAbstracts($solrDoc['page_notes_abstracts']);
        }

        if (isset($solrDoc['page_notes_abstracts_ids']) && !empty($solrDoc['page_notes_abstracts_ids'])) {
            $page->setPageNotesAbstractsIds($solrDoc['page_notes_abstracts_ids']);
        }

        if (isset($solrDoc['page_notes_abstracts']) && !empty($solrDoc['page_notes_abstracts'])) {
            $page->setPageNotesAbstracts($solrDoc['page_notes_abstracts']);
        }

        if (isset($solrDoc['page_notes_abstracts_ids']) && !empty($solrDoc['page_notes_abstracts_ids'])) {
            $page->setPageNotesAbstractsIds($solrDoc['page_notes_abstracts_ids']);
        }

        if (isset($solrDoc['page_works']) && !empty($solrDoc['page_works'])) {
            $page->setPageWorks($solrDoc['page_works']);
        }

        if (isset($solrDoc['page_works_ids']) && !empty($solrDoc['page_works_ids'])) {
            $page->setPageWorksIds($solrDoc['page_works_ids']);
        }

        if (isset($solrDoc['page_entities']) && !empty($solrDoc['page_entities'])) {
            $page->setPageEntities($solrDoc['page_entities']);
        }

        if (isset($solrDoc['page_entities_ids']) && !empty($solrDoc['page_entities_ids'])) {
            $page->setPageEntitiesIds($solrDoc['page_entities_ids']);
        }

        if (isset($solrDoc['page_entities_types']) && !empty($solrDoc['page_entities_types'])) {
            $page->setPageEntitiesTypes($solrDoc['page_entities_types']);
        }

        if (isset($solrDoc['page_dates']) && !empty($solrDoc['page_dates'])) {
            $page->setPageDates($solrDoc['page_dates']);
        }

        if (isset($solrDoc['page_dates_ids']) && !empty($solrDoc['page_dates_ids'])) {
            $page->setPageDatesIds($solrDoc['page_dates_ids']);
        }

        if (isset($solrDoc['page_all_annotation_ids']) && !empty($solrDoc['page_all_annotation_ids'])) {
            $page->setPageAllAnnotationIds($solrDoc['page_all_annotation_ids']);
        }

        $page->setAnnotationCollectionLabel(
            'Annotations for Goethes Farbenlehre in Berlin - Dokument: '
            . $page->getArticleId() . ' Seite: ' . $page->getId()
        );

        return $page;
    }

    public function getArticleById(string $id): ?ArticleInterface
    {
        $article = new GflArticle();
        $solrDocument = $this->getDocument($id);

        $article->setId($solrDocument['id']);
        $article->setTitle($solrDocument['short_title'] ?? null);
        $article->setEditedText($solrDocument['edited_text'] ?? '');
        $article->setTranscriptedText($solrDocument['transcripted_text'] ?? '');
        $article->setPageIds($solrDocument['page_ids'] ?? '');

        if (isset($solrDocument['author']) && !empty($solrDocument['author'])) {
            $article->setAuthor(implode(', ', $solrDocument['author']));
        }

        if (isset($solrDocument['recipient']) && !empty($solrDocument['recipient'])) {
            $article->setRecipient(implode(', ', $solrDocument['recipient']));
        }

        if (isset($solrDocument['origin_place']) && !empty($solrDocument['origin_place'])) {
            $article->setOriginPlace(implode(', ', $solrDocument['origin_place']));
        }

        if (isset($solrDocument['destination_place']) && !empty($solrDocument['destination_place'])) {
            $article->setDestinationPlace(implode(', ', $solrDocument['destination_place']));
        }

        if (isset($solrDocument['article_pub_date']) && !empty($solrDocument['article_pub_date'])) {
            $article->setPublishDate($solrDocument['article_pub_date']);
        }

        if (isset($solrDocument['responses']) && !empty($solrDocument['responses'])) {
            $article->setResponses($solrDocument['responses']);
        }

        if (isset($solrDocument['entities']) && !empty($solrDocument['entities'])) {
            $article->setEntities($solrDocument['entities']);
        }

        if (isset($solrDocument['page_segs']) && !empty($solrDocument['page_segs'])) {
            $article->setPageSegs($solrDocument['page_segs']);
        }

        if (isset($solrDocument['page_notes']) && !empty($solrDocument['page_notes'])) {
            $article->setPageNotes($solrDocument['page_notes']);
        }

        if (isset($solrDocument['page_notes_ids']) && !empty($solrDocument['page_notes_ids'])) {
            $article->setPageNotesIds($solrDocument['page_notes_ids']);
        }

        if (isset($solrDocument['page_notes_abstracts']) && !empty($solrDocument['page_notes_abstracts'])) {
            $article->setPageNotesAbstracts($solrDocument['page_notes_abstracts']);
        }

        if (isset($solrDocument['page_notes_abstracts_ids']) && !empty($solrDocument['page_notes_abstracts_ids'])) {
            $article->setPageNotesAbstractsIds($solrDocument['page_notes_abstracts_ids']);
        }

        if (isset($solrDocument['page_notes_abstracts']) && !empty($solrDocument['page_notes_abstracts'])) {
            $article->setPageNotesAbstracts($solrDocument['page_notes_abstracts']);
        }

        if (isset($solrDocument['page_notes_abstracts_ids']) && !empty($solrDocument['page_notes_abstracts_ids'])) {
            $article->setPageNotesAbstractsIds($solrDocument['page_notes_abstracts_ids']);
        }

        if (isset($solrDocument['page_works']) && !empty($solrDocument['page_works'])) {
            $article->setPageWorks($solrDocument['page_works']);
        }

        if (isset($solrDocument['page_works_ids']) && !empty($solrDocument['page_works_ids'])) {
            $article->setPageWorksIds($solrDocument['page_works_ids']);
        }

        if (isset($solrDocument['page_entities']) && !empty($solrDocument['page_entities'])) {
            $article->setPageEntities($solrDocument['page_entities']);
        }

        if (isset($solrDocument['page_entities_ids']) && !empty($solrDocument['page_entities_ids'])) {
            $article->setPageEntitiesIds($solrDocument['page_entities_ids']);
        }

        if (isset($solrDocument['page_entities_types']) && !empty($solrDocument['page_entities_types'])) {
            $article->setPageEntitiesTypes($solrDocument['page_entities_types']);
        }

        if (isset($solrDocument['page_dates']) && !empty($solrDocument['page_dates'])) {
            $article->setPageDates($solrDocument['page_dates']);
        }

        if (isset($solrDocument['page_dates_ids']) && !empty($solrDocument['page_dates_ids'])) {
            $article->setPageDatesIds($solrDocument['page_dates_ids']);
        }

        if (isset($solrDocument['page_all_annotation_ids']) && !empty($solrDocument['page_all_annotation_ids'])) {
            $article->setPageAllAnnotationIds($solrDocument['page_all_annotation_ids']);
        }

        if (isset($solrDocument['image_license']) && !empty($solrDocument['image_license'])) {
            $article->setImageLicense($solrDocument['image_license']);
        }

        if (isset($solrDocument['image_license_url']) && !empty($solrDocument['image_license_url'])) {
            $article->setImageLicenseLink($solrDocument['image_license_url']);
        }

        if (isset($solrDocument['page_foliant']) && !empty($solrDocument['page_foliant'])) {
            $article->setPageFoliant($solrDocument['page_foliant']);
        }

        if (isset($solrDocument['leopoldina_edition']) && !empty($solrDocument['leopoldina_edition'])) {
            $article->setLeopoldinaEdition($solrDocument['leopoldina_edition']);
        }

        if (isset($solrDocument['print_source']) && !empty($solrDocument['print_source'])) {
            $article->setPrintSource($solrDocument['print_source']);
        }

        $article->setLicense($solrDocument['license']);;
        $article->setLanguage($solrDocument['language']);
        $article->setImageUrl($solrDocument['image_url']);
        $article->setPageNumber((string)($solrDocument['number_of_pages'] ?: $solrDocument['page_number']));
        $article->setGndKeywords($solrDocument['gnd_keyword']);
        $article->setFreeKeywords($solrDocument['free_keyword']);
        $article->setInstitution($solrDocument['institution']);
        $article->setShelfmark($solrDocument['shelfmark']);
        $article->setScriptSource($solrDocument['script_source']);
        $article->setWriter($solrDocument['writer']);
        $article->setReferences($solrDocument['references']);
        $article->setRelatedItems($solrDocument['related_items']);

        $this->setArticle($article);

        return $article;
    }

    public function getMetadata(string $id): ?array
    {
        if (!$this->article) {
            $article = $this->getArticleById($id);

            /** @var GflArticle $article */
            $this->setArticle($article);
        } else {
            $article = $this->article;
        }

        if (null !== $article->getPrintSource()) {
            $metadata[] = ['key' => $this->i18n->trans('Print_Source', [], 'messages'), 'value' => $article->getPrintSource()];
        }

        if (null !== $article->getLeopoldinaEdition()) {
            $metadata[] = ['key' => $this->i18n->trans('Leopoldina_Edition', [], 'messages'), 'value' => $article->getLeopoldinaEdition()];
        }

        if (null !== $article->getAuthor()) {
            $metadata[] = ['key' => $this->i18n->trans('Author', [], 'messages'), 'value' => $article->getAuthor()];
        }

        if (null !== $article->getPublishDate()) {
            $metadata[] = ['key' => $this->i18n->trans('Publish_Date', [], 'messages'), 'value' => $article->getPublishDate()];
        }

        if (null !== $article->getOriginPlace()) {
            $metadata[] = ['key' => $this->i18n->trans('Origin_Place', [], 'messages'), 'value' => $article->getOriginPlace()];
        }

        if (null !== $article->getRecipient()) {
            $metadata[] = ['key' => $this->i18n->trans('Recipient', [], 'messages'), 'value' => $article->getRecipient()];
        }

        if (null !== $article->getDestinationPlace()) {
            $metadata[] = ['key' => $this->i18n->trans('Destination_Place', [], 'messages'), 'value' => $article->getDestinationPlace()];
        }

        if (null !== $article->getInstitution()) {
            $metadata[] = ['key' => $this->i18n->trans('Institution', [], 'messages'), 'value' => $article->getInstitution()];
        }

        if (null !== $article->getShelfmark()) {
            $metadata[] = ['key' => $this->i18n->trans('Shelfmark', [], 'messages'), 'value' => $article->getShelfmark()];
        }

        if (null !== $article->getScriptSource()) {
            $metadata[] = ['key' => $this->i18n->trans('Script_Source', [], 'messages'), 'value' => $article->getScriptSource()];
        }

        if (null !== $article->getWriter()) {
            if (is_array($article->getWriter()) && !empty($article->getWriter())) {
                $writers = implode('<br> ', $article->getWriter());
            }

            $metadata[] = ['key' => $this->i18n->trans('Writer', [], 'messages'), 'value' => $writers ?? ''];
        }

        if (null !== $article->getReferences()) {
            $metadata[] = [
                'key' => $this->i18n->trans('Reference', [], 'messages'),
                'value' => '',
                'metadata' => array_map(function ($item) {
                    return json_decode($item);
                }, $article->getReferences())
            ];
        }

        if (null !== $article->getResponses()) {
            $metadata[] = [
                'key' => $this->i18n->trans('Response', [], 'messages'),
                'value' => '',
                'metadata' => array_map(function ($item) {
                    return json_decode($item);
                }, $article->getResponses())
            ];
        }

        if (null !== $article->getRelatedItems()) {
            $metadata[] = [
                'key' => $this->i18n->trans('Related_Items', [], 'messages'),
                'value' => '',
                'metadata' => array_map(function ($item) {
                    return json_decode($item);
                }, $article->getRelatedItems())
            ];
        }

        if (null !== $article->getGndKeywords()) {
            $metadata[] = ['key' => $this->i18n->trans('Keywords_gnd', [], 'messages'), 'value' => implode('; ', $article->getGndKeywords())];
        }

        if (null !== $article->getFreeKeywords()) {
            $metadata[] = ['key' => $this->i18n->trans('Keywords_free', [], 'messages'), 'value' => implode('; ', $article->getFreeKeywords())];
        }

        if (null !== $article->getLicense()) {
            $metadata[] = ['key' => $this->i18n->trans('Edition_license', [], 'messages'), 'value' => $article->getLicense()];
        }

        $teiDocumentLink = $this->mainDomain . $this->router->generate('_teifile', ['filename' => $article->getId()]);
        $metadata[] = ['key' => $this->i18n->trans('TEI document', [], 'messages'), 'value' => 'via REST (' . $teiDocumentLink . ')'];

        return $metadata;
    }

    public function getEntity(string $entityGnd): ?array
    {
        $query = $this->client->createSelect()
            ->setQuery(sprintf('id:%s', $entityGnd));
        $select = $this->client->select($query);
        $count = $select->count();

        if (0 === $count) {
            return [];
        }

        return $select->getDocuments()[0]->getFields();
    }

    public function getItemAnnotationsStartIndex(string $id, int $pageNumber): int
    {
        $query = $this->client->createSelect()
            ->setFields(['page_entities'])
            ->setQuery(sprintf('id:%s', $id));
        $select = $this->client->select($query);
        $numFound = $select->getNumFound();
        $query->setRows($numFound);
        $select = $this->client->select($query);
        $count = $select->count();

        if (0 === $count) {
            throw new \InvalidArgumentException(sprintf('No entity found for the document %s', $id));
        }

        $startIndex = 0;
        foreach ($select->getDocuments() as $key => $entitySet) {
            if (($key < ($pageNumber - 1)) && isset($entitySet->getFields()['page_entities'])) {
                $startIndex += count($entitySet->getFields()['page_entities']);
            }
        }

        return $startIndex + 1;
    }

    public function getTotalNumberOfAnnotationsFromArticle(string $id): int
    {
        $query = $this->client->createSelect()
            ->setFields(['entities'])
            ->setQuery(sprintf('article_id:%s', $id));
        $select = $this->client->select($query);
        $numFound = $select->getNumFound();
        $query->setRows($numFound);
        $select = $this->client->select($query);
        $count = $select->count();

        if (0 === $count) {
            throw new \InvalidArgumentException(sprintf('No entity found for the document %s', $id));
        }

        $total = 0;
        foreach ($select->getDocuments() as $entitySet) {
            if (isset($entitySet->getFields()['entities'])) {
                $total += count($entitySet->getFields()['entities']);
            }
        }

        return $total;
    }

    public function getManifestUrlByPageId(string $pageId): string
    {
        $solrDocument = $this->getDocument($pageId);

        return $solrDocument['article_id'];
    }

    public function getSupportCssUrl(string $documentId): string
    {
        return $this->mainDomain . $this->packages->getUrl('build/support.css');
    }

    private function getDocument(string $id): \Solarium\Core\Query\DocumentInterface
    {
        $query = $this->client->createSelect()->setQuery(sprintf('id:%s', $id));
        $select = $this->client->select($query);
        $count = $select->count();

        if (0 === $count) {
            throw new \InvalidArgumentException(sprintf('Document %s not found', $id));
        }

        return $select->getDocuments()[0];
    }

    public function getAnnotationItems(PageInterface $page): array
    {
        /** @var GflPage $page $items */
        $items = [];

        if (!empty($page->getPageNotesAbstracts())) {
            foreach ($page->getPageNotesAbstracts() as $key => $pageAbstract) {
                if (isset($page->getPageNotesAbstractsIds()[$key]) && !empty($page->getPageNotesAbstractsIds()[$key])) {
                    $item = new AnnotationItem();
                    $item->setBody($this->getAnnotationBody($pageAbstract, 'Abstract'));
                    $item->setTarget(
                        $this->getAnnotationTarget(
                            $page->getId(),
                            $page->getPageNotesAbstractsIds()[$key],
                            false
                        )
                    );
                    $id = $this->createAnnotationId($page->getId(), $page->getPageNotesAbstractsIds()[$key]);
                    $item->setId($id);
                    $items[] = $item;
                }
            }
        }

        if (!empty($page->getPageEntities())) {
            foreach ($page->getPageEntities() as $key => $pageEntity) {
                $item = new AnnotationItem();
                $item->setBody($this->getAnnotationBody($pageEntity, $page->getPageEntitiesTypes()[$key]));
                $item->setTarget($this->getAnnotationTarget($page->getId(), $page->getPageEntitiesIds()[$key]));
                $id = $this->createAnnotationId($page->getId(), $page->getPageEntitiesIds()[$key]);
                $item->setId($id);
                $items[] = $item;
            }
        }

        if (!empty($page->getPageNotes())) {
            foreach ($page->getPageNotes() as $key => $pageNote) {
                if (isset($page->getPageNotesIds()[$key]) && !empty($page->getPageNotesIds()[$key])) {
                    $item = new AnnotationItem();
                    $item->setBody($this->getAnnotationBody($pageNote, 'Editorial Comment'));
                    $item->setTarget($this->getAnnotationTarget($page->getId(), $page->getPageNotesIds()[$key]));
                    $id = $this->createAnnotationId($page->getId(), $page->getPageNotesIds()[$key]);
                    $item->setId($id);
                    $items[] = $item;
                }
            }
        }

        if (!empty($page->getPageDates())) {
            foreach ($page->getPageDates() as $key => $pageDate) {
                if (isset($page->getPageDatesIds()[$key]) && !empty($page->getPageDatesIds()[$key])) {
                    $item = new AnnotationItem();
                    $item->setBody($this->getAnnotationBody($pageDate, 'Date'));
                    $item->setTarget($this->getAnnotationTarget($page->getId(), $page->getPageDatesIds()[$key]));
                    $id = $this->createAnnotationId($page->getId(), $page->getPageDatesIds()[$key]);
                    $item->setId($id);
                    $items[] = $item;
                }
            }
        }

        if (!empty($page->getPageWorks())) {
            foreach ($page->getPageWorks() as $key => $pageWork) {
                if (isset($page->getPageWorksIds()[$key]) && !empty($page->getPageWorksIds()[$key])) {
                    $item = new AnnotationItem();
                    $item->setBody($this->getAnnotationBody($pageWork, 'Work'));
                    $item->setTarget($this->getAnnotationTarget($page->getId(), $page->getPageWorksIds()[$key]));
                    $id = $this->createAnnotationId($page->getId(), $page->getPageWorksIds()[$key]);
                    $item->setId($id);
                    $items[] = $item;
                }
            }
        }

        // Sort items according to page_all_annotation_ids
        uasort($items, function (AnnotationItem $a, AnnotationItem $b) use ($page) {
            $sortingIndexA = 0;
            $sortingIndexB = 0;
            foreach ($page->getPageAllAnnotationIds() as $index => $solrId) {
                $tempAnnotationId = $this->createAnnotationId($page->getId(), $solrId);

                if ($a->getId() === $tempAnnotationId) {
                    $sortingIndexA = $index;
                } elseif ($b->getId() === $tempAnnotationId) {
                    $sortingIndexB = $index;
                }
            }

            if ($sortingIndexA < $sortingIndexB) {
                return -1;
            } else {
                return 1;
            }
        });

        // uasort doesn't set the correct index keys, so we  have to fix this
        $items = array_values($items);

        return $items;
    }

    private function createAnnotationId(string $documentId, string $solrId) {
        return $this->mainDomain.'/'.$documentId.'/'.$solrId;
    }

    private function getAnnotationBody(string $value, string $type): Body
    {
        $body = new Body();
        $body->setValue($value);
        $body->setXContentType($type);

        return $body;
    }

    private function getAnnotationTarget($documentId, $annotationId, $hasSelector = true): Target
    {
        $target = new Target();

        $target->setId($this->createAnnotationId($documentId, $annotationId));

        if ($hasSelector) {
            $selector = new Selector();
            $selector->setType('CssSelector');
            $selector->setValue('#'.$annotationId);
            $target->setSelector($selector);
        }

        $target->setFormat('text/xml');
        $target->setLanguage('ger');

        return $target;
    }

}
