<?php

namespace App\Service;

use League\Flysystem\FilesystemOperator;

/**
 * Wrapper around cache and source filesystems.
 */
class FileService
{
    private FilesystemOperator  $cacheFilesystem;
    private FilesystemOperator  $imageFilesystem;
    private FilesystemOperator  $mainFilesystem;
    private FilesystemOperator  $pdfFilesystem;
    private FilesystemOperator  $sourceFilesystem;
    private FilesystemOperator  $teiFilesystem;

    public function __construct(FilesystemOperator  $mainFilesystem, FilesystemOperator $cacheFilesystem, FilesystemOperator  $sourceFilesystem, FilesystemOperator  $pdfFilesystem, FilesystemOperator  $imageFilesystem, FilesystemOperator  $teiFilesystem)
    {
        $this->mainFilesystem = $mainFilesystem;
        $this->cacheFilesystem = $cacheFilesystem;
        $this->sourceFilesystem = $sourceFilesystem;
        $this->pdfFilesystem = $pdfFilesystem;
        $this->imageFilesystem = $imageFilesystem;
        $this->teiFilesystem = $teiFilesystem;
    }

    public function getCacheFilesystem(): FilesystemOperator
    {
        return $this->cacheFilesystem;
    }

    public function getImageFilesystem(): FilesystemOperator
    {
        return $this->imageFilesystem;
    }

    public function getMainFilesystem(): FilesystemOperator
    {
        return $this->mainFilesystem;
    }

    public function getPdfFilesystem(): FilesystemOperator
    {
        return $this->pdfFilesystem;
    }

    public function getSourceFilesystem(): FilesystemOperator
    {
        return $this->sourceFilesystem;
    }

    public function getTeiFilesystem(): FilesystemOperator
    {
        return $this->teiFilesystem;
    }
}
