<?php

namespace App\Service;

use Doctrine\Common\Collections\ArrayCollection;
use Solarium\Client;
use Solarium\QueryType\Select\Query\FilterQuery;

/**
 * Class SolrSearchService.
 */
class SolrSearchService implements SearchServiceInterface
{
    private \Solarium\Client $client;

    /**
     * SolrSearchService constructor.
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function addFilter(?array $facets): ArrayCollection
    {
        $filterQueries = new ArrayCollection();

        if ($facets && is_array($facets)) {
            foreach ($facets as $facet) {
                if (is_array($facet)) {
                    foreach ($facet as $key => $filterQuery) {
                        $filter = new FilterQuery();
                        $filter->setKey(sha1(microtime().$key));
                        if (preg_match('/\[[a-zA-Z0-9_]* TO [a-zA-Z0-9_]*\]/', $filterQuery)) {
                            $filter->setQuery(vsprintf('%s:%s', [$key, $filterQuery]));
                        } else {
                            $filter->setQuery(vsprintf('%s:"%s"', [$key, $filterQuery]));
                        }
                        $filterQueries->add($filter);
                    }
                }
            }
        }

        return $filterQueries;
    }

    public function getDateFromDocument(array $originDate): array
    {
        array_walk($originDate, function (&$val) {
            $val = substr($val, 0, 4);
        });

        $from = min(array_unique($originDate));
        $to = max(array_unique($originDate));

        return [$from, $to];
    }

    public function getDateFromFilter(array $originDate): array
    {
        $originDate = implode('TO', $originDate);
        $originDate = explode('TO', rtrim(ltrim($originDate, '['), ']'));
        $from = rtrim($originDate[0]);
        $to = ltrim($originDate[1]);

        return [$from, $to];
    }

    public function getDocumentsFromEntities(string $gndId): array
    {
        $select = $this->client->createSelect()->setRows(2000);
        $query = "all_entities:$gndId AND doctype:article";
        $select->setQuery($query)->addSort('origin_date', 'asc');

        return $this->client->select($select)->getData()['response']['docs'];
    }

    public function getDocumentsFromLiterature(string $litId): array
    {
        $select = $this->client->createSelect()->setRows(2000);
        $query = "all_literature:\"$litId\" AND doctype:article";
        $select->setQuery($query)->addSort('origin_date', 'asc');

        return $this->client->select($select)->getData()['response']['docs'];
    }

    public function getEntities(): array
    {
        $select = $this->client->createSelect()->setRows(2000);
        $query = vsprintf('%s:%s', ['doctype', 'entity']);
        $select->setQuery($query)->addSort('entity_name', 'asc');

        return $this->client->select($select)->getData()['response']['docs'];
    }

    public function getEntitiesById(string $gndId): array
    {
        $select = $this->client->createSelect();
        $query = "id:\"$gndId\" AND doctype:entity";
        $select->setQuery($query)->addSort('entity_name', 'asc');

        return $this->client->select($select)->getData()['response']['docs'];
    }

    public function getFacets(array $facetsArr): array
    {
        $facets = [];

        foreach ($facetsArr as $facetName => $facetItems) {
            $facet = [];
            foreach (array_chunk($facetItems, 2) as $facetItem) {
                list($facetItemName, $facetItemCount) = $facetItem;
                $facet[$facetItemName] = $facetItemCount;
            }
            $facets[$facetName] = $facet;
        }

        return $facets;
    }

    public function getInfo(): array
    {
        $select = $this->client->createSelect();
        $facetSet = $select->getFacetSet();
        $facetSet->createFacetField('doctype')->setField('doctype');
        $facetSet->createFacetField('author')->setField('author_facet');
        $facetSet->createFacetField('recipient')->setField('recipient_facet');
        $facetSet->createFacetField('origin_place')->setField('origin_place_facet');
        $facetSet->createFacetField('destination_place')->setField('destination_place_facet');
        $select->setQuery('*');

        return $this->client->select($select)->getData();
    }

    public function getLiterature(): array
    {
        $select = $this->client->createSelect()->setRows(1000);
        $query = vsprintf('%s:%s', ['doctype', 'literature']);
        $select->setQuery($query)->addSort('id', 'asc');

        return $this->client->select($select)->getData()['response']['docs'];
    }

    public function getLiteratureItem(string $id): array
    {
        $select = $this->client->createSelect()->setRows(1000);
        $query = vsprintf('%s:%s AND %s:"%s"', ['doctype', 'literature', 'id', $id]);
        $select->setQuery($query);

        return $this->client->select($select)->getData()['response']['docs'][0];
    }

    public function getNumberOfAuthors(): array
    {
        $select = $this->client->createSelect()->setRows(200);
        $query = vsprintf('%s:%s', ['author', '*']);
        $select->setQuery($query);

        return $this->client->select($select)->getData();
    }

    public function getNumberOfAuthorsSort(): array
    {
        $select = $this->client->createSelect()->setRows(200);
        $query = vsprintf('%s:%s&%s=%s+%s', ['author', '*', 'sort', 'author', 'asc']);
        $select->setQuery($query);

        return $this->client->select($select)->getData();
    }

    public function getPageData(string $imagePath): array
    {
        $select = $this->client->createSelect();
        $query = vsprintf('%s:%s', ['image_url', $imagePath]);
        $select->setQuery($query);

        $pageData = [];
        if (1 === $this->client->select($select)->count()) {
            $pageData = $this->client->select($select)->getData()['response']['docs'][0];
        }

        return $pageData;
    }

    public function search(array $query, ?string $sort, ?string $order, ?string $searchNotes): array
    {
        $select = $this->client->createSelect();

        $queryString = '(';

        foreach ($query as $index => $queryItem) {
            $operator = $queryItem['operator'] ?? null;
            $field = $queryItem['field'] ?? null;
            $value = $queryItem['value'] ?? null;
            $phrase = $queryItem['phrase'] ?? null;

            if (isset($operator) && !empty($operator) && 'none' !== $operator) {
                if ('NOT' === $operator) {
                    if ($index > 0) {
                        $queryString .= ' AND ';
                    }

                    $queryString .= "($field:* NOT ";
                } else {
                    $queryString .= " $operator ";
                }
            }

            if (isset($field) && !empty($field) && isset($value)) {
                $value = $this->escapeValue($value);
                $containsSpecialChars = preg_match('/\*|\?/', $value);

                if ('origin_date' === $field) {
                    $value = "[$value-01-01T00:00:00Z TO $value-12-31T00:00:00Z]";
                    $queryString .= "$field:$value";
                }
                else if ('id' === $field) {
                    $queryString .= "search_id:$value";
                }
                else {
                    if ('' === $value || '*' === $value) {
                        $value = '*';
                    } else {
                        $isPhrase = isset($phrase) && 'on' === $phrase;
                        $valueTemp = $isPhrase ? '"' : '';
                        $valueTemp .= $containsSpecialChars > 0 || $isPhrase ? $value : "*$value*";
                        $valueTemp .= $isPhrase ? '"' : '';
                        $value = $valueTemp;
                    }
                    $queryString .= "$field:$value";
                }
            }

            if ('NOT' === $operator) {
                $queryString .= ')';
            }
        }

        $queryString .= ') AND doctype:article';

        $select->setQuery($queryString);

        if ($sort) {
            $select->setSorts([$sort => $order]);
        } else {
            $select->setSorts(['origin_date' => 'asc']);
        }

        $numFound = $this->client->select($select)->getNumFound();
        $select->setRows($numFound);

        return $this->client->select($select)->getData();
    }

    private function escapeValue(string $value): string
    {
        $pattern = ['/\s/', '/\(/', '/\)/', '/\:/', '/\</', '/\>/', '/\&/', '/\'/', '/\"/', '/\?/', '/\!/'];
        $replacements = ['\ ', '\(', '\)', '\:', '\<', '\>', '\&', '\'', '\"', '\?', '\!'];

        return preg_replace($pattern, $replacements, $value);
    }
}
