<?php

declare(strict_types=1);

namespace App\Transform;

use DateTime;
use DOMElement;
use DOMNodeList;
use DOMXPath;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\RouterInterface;

class MetadataTransformer implements MetadataTransformerInterface
{
    private ?array $documentLanguages;
    private ?string $mainDomain;
    private RouterInterface $router;

    public function __construct(RouterInterface $router, ParameterBagInterface $params)
    {
        $this->router = $router;
        $this->mainDomain = $params->get('main_domain');
        $this->documentLanguages = $params->get('document_languages');
    }

    public function getAuthor(DOMXPath $xpath): string
    {
        $author = '';
        $authorNode = $xpath->query('//tei:name[@type="person" and @subtype="aut"]');
        if ($authorNode->item(0)) {
            $author = $authorNode->item(0)->nodeValue;
            $author = trim(preg_replace('/\s+/', ' ', $author));
        }

        return $author;
    }

    public function getCountry(DOMXPath $xpath): string
    {
        $country = '';
        $countryNode = $xpath->query('//tei:country');
        if ($countryNode->item(0)) {
            $country = $countryNode->item(0)->nodeValue;
        }

        return $country;
    }

    public function getDestinationPlace(DOMXPath $xpath): string
    {
        $destinationPlace = '';
        $destinationPlaceNode = $xpath->query('//tei:name[@type="place" and @subtype="dtn"]');
        if ($destinationPlaceNode->item(0)) {
            $destinationPlace = $destinationPlaceNode->item(0)->nodeValue;
        }

        return $destinationPlace;
    }

    public function getFreeKeywords(DOMXPath $xpath): array
    {
        $freeKeywords = [];
        $freeKeyWordNodes = $xpath->query('//tei:keywords[@scheme = "frei"]/tei:term');
        if (is_iterable($freeKeyWordNodes)) {
            foreach ($freeKeyWordNodes as $freeKeyWordNode) {
                if (!empty($freeKeyWordNode->nodeValue)) {
                    $freeKeyword = trim(preg_replace('/\s+/', ' ', $freeKeyWordNode->nodeValue));
                    $freeKeywords[] = $freeKeyword;
                }
            }
        }

        $freeKeyWordNodes = $xpath->query('//tei:keywords[@scheme = "free"]/tei:term');
        if (is_iterable($freeKeyWordNodes)) {
            foreach ($freeKeyWordNodes as $freeKeyWordNode) {
                if (!empty($freeKeyWordNode->nodeValue)) {
                    $freeKeyword = trim(preg_replace('/\s+/', ' ', $freeKeyWordNode->nodeValue));
                    $freeKeywords[] = $freeKeyword;
                }
            }
        }

        return $freeKeywords;
    }

    public function getGndKeywords(DOMXPath $xpath): array
    {
        $gndKeywords = [];
        $gndKeyWordNodes = $xpath->query('//tei:keywords[@scheme = "#gnd"]/tei:term');
        if (is_iterable($gndKeyWordNodes)) {
            foreach ($gndKeyWordNodes as $gndKeyWordNode) {
                if (!empty($gndKeyWordNode->nodeValue)) {
                    $gndKeyword = trim(preg_replace('/\s+/', ' ', $gndKeyWordNode->nodeValue));
                    $gndKeywords[] = $gndKeyword;
                }
            }
        }

        return $gndKeywords;
    }

    public function getId(DOMXPath $xpath): string
    {
        $id = '';
        $idNode = $xpath->query('//tei:text/@xml:id');
        $id = $idNode->item(0)->nodeValue;

        return $id;
    }

    public function getImageIds(DOMXPath $xpath): array
    {
        $imageIds = [];
        $imageIdsNodes = $xpath->query('//tei:graphic/@xml:id');
        if (is_iterable($imageIdsNodes)) {
            foreach ($imageIdsNodes as $imageIdsNode) {
                if (!empty($imageIdsNode->nodeValue)) {
                    $imageId = trim(preg_replace('/\s+/', ' ', $imageIdsNode->nodeValue));
                    $imageIds[] = $imageId;
                }
            }
        }

        return $imageIds;
    }

    public function getImageUrls(DOMXPath $xpath): array
    {
        $imageUrls = [];
        $imageUrlssNodes = $xpath->query('//tei:graphic/@url');
        if (is_iterable($imageUrlssNodes)) {
            foreach ($imageUrlssNodes as $imageUrlssNode) {
                if (!empty($imageUrlssNode->nodeValue)) {
                    $imageUrl = trim(preg_replace('/\s+/', ' ', $imageUrlssNode->nodeValue));
                    $imageUrls[] = $imageUrl;
                }
            }
        }

        return $imageUrls;
    }

    public function getInstitution(DOMXPath $xpath): string
    {
        $institution = '';
        $institutionNode = $xpath->query('//tei:institution');
        if ($institutionNode->item(0)) {
            $institution = $institutionNode->item(0)->nodeValue;
        }

        return $institution;
    }

    public function getLanguage(DOMXPath $xpath): string
    {
        $language = '';
        $languageNode = $xpath->query('//tei:text//@xml:lang');
        if ($languageNode->item(0)) {
            $language = $languageNode->item(0)->nodeValue;
            $language = $this->documentLanguages[$language];
        }

        return $language;
    }

    public function getLeopoldinaEdition(DOMXPath $xpath): string
    {
        /**
         * @var DOMNodeList $nodes
         */
        $nodes = $xpath->query('//tei:sourceDesc//tei:relatedItem[@type = "print"]/tei:bibl');
        $result = '';
        if (0 === count($nodes)) {
            return $result;
        }
        foreach ($nodes[0]->childNodes as $childNode) {
            /* @var DOMElement $node */
            if ('ref' === $childNode->nodeName) {
                $target = $childNode->getAttribute('target');
            }
        }

        return $nodes[0]->textContent . (!empty($target) ? (' (' . $target . ')') : '');
    }

    public function getLicense(DOMXPath $xpath): string
    {
        $license = '';
        $licenseNode = $xpath->query('//tei:licence');
        if ($licenseNode->item(0)) {
            $license = $licenseNode->item(0)->nodeValue;
        }

        return $license;
    }

    public function getNumberOfPages(DOMXPath $xpath): ?int
    {
        $numberOfPages = null;
        $numberOfPagesNode = $xpath->query('//tei:body//tei:pb');
        if ($numberOfPagesNode->count()) {
            $numberOfPages = $numberOfPagesNode->count();
        }

        return $numberOfPages;
    }

    public function getOriginDate(DOMXPath $xpath): string
    {
        $nodes = $xpath->query('//tei:title[@type = "desc"]/tei:date');

        if (is_iterable($nodes) && count($nodes) > 0) {
            /** @var DOMElement $node */
            $node = $nodes[0];

            if ($node->hasAttribute('when')) {
                return $this->convertDate($node->getAttribute('when'));
            }
            if ($node->hasAttribute('from')) {
                return $this->convertDate($node->getAttribute('from'));
            }

            if ($node->hasAttribute('notBefore')) {
                return $this->convertDate($node->getAttribute('notBefore'));
            }

            if ($node->hasAttribute('to')) {
                return $this->convertDate($node->getAttribute('to'));
            }

            if ($node->hasAttribute('notAfter')) {
                return $this->convertDate($node->getAttribute('notAfter'));
            }
        }

        return $this->convertDate('0000-01-01');
    }

    public function getOriginPlace(DOMXPath $xpath): string
    {
        $originPlace = '';
        $originPlaceNode = $xpath->query('//tei:name[@type="place" and @subtype="orn"]');

        if ($originPlaceNode->item(0)) {
            $originPlace = $originPlaceNode->item(0)->nodeValue;
        }

        return $originPlace;
    }

    public function getPrintSource(DOMXPath $xpath): ?string
    {
        /**
         * @var DOMNodeList $nodes
         */
        $nodes = $xpath->query('//tei:sourceDesc/tei:bibl');

        if (0 === count($nodes)) {
            return null;
        }

        $target = $nodes[0]->getAttribute('corresp');

        if (empty($target)) {
            return null;
        }

        $litId = explode('#', $target)[1];

        if (!isset($litId)) {
            return null;
        }

        $key = $this->mainDomain . $this->router->generate('_literature') . '/' . $litId;

        $pages = '';
        foreach ($nodes[0]->childNodes as $childNode) {
            /* @var DOMElement $node */
            if ('biblScope' === $childNode->nodeName) {
                $pages = $childNode->textContent;
            }
        }

        $value = str_replace('_', ' ', $litId) . ', ' . $pages;

        return "$value ($key)";
    }

    public function getPublicationDate(DOMXPath $xpath): string
    {
        $publicationDate = '';
        $publicationDateNode = $xpath->query('//date[@type = "orn"]');
        if ($publicationDateNode->item(0)) {
            $publicationDate = $publicationDateNode->item(0)->nodeValue;
        }

        return $publicationDate;
    }

    public function getRecipient(DOMXPath $xpath): string
    {
        $recipient = '';
        $recipientNode = $xpath->query('//tei:name[@type="person" and @subtype="rcp"]');
        if ($recipientNode->item(0)) {
            $recipient = $recipientNode->item(0)->nodeValue;
            $recipient = trim(preg_replace('/\s+/', ' ', $recipient));
        }

        return $recipient;
    }

    public function getReferences(DOMXPath $xpath): array
    {
        $referenceNodes = $xpath->query('//tei:relatedItem[@type = "letter" and @subtype = "related"]/tei:ref');

        return $this->transformRelatedItems($referenceNodes);
    }

    public function getRelatedItems(DOMXPath $xpath): array
    {
        $relatedItemNodes = $xpath->query('//tei:relatedItem[@type="letter" and not(@subtype)]//tei:ref');

        return $this->transformRelatedItems($relatedItemNodes);
    }

    public function getRepository(DOMXPath $xpath): string
    {
        $repository = '';
        $repositoryNode = $xpath->query('//tei:repository');
        if ($repositoryNode->item(0)) {
            $repository = $repositoryNode->item(0)->nodeValue;
        }

        return $repository;
    }

    public function getResponses(DOMXPath $xpath): array
    {
        $responseNodes = $xpath->query('//tei:relatedItem[@type = "letter" and @subtype = "response"]/tei:ref');

        return $this->transformRelatedItems($responseNodes);
    }

    public function getScriptSource(DOMXPath $xpath): string
    {
        $scriptSource = '';
        $supportDescNode = $xpath->query('//tei:supportDesc/tei:extent/text()');
        if (!empty($supportDescNode->item(0)->data)) {
            $scriptSource .= trim(preg_replace('/\s+/', ' ', $supportDescNode->item(0)->data)) . ' ';
        }

        $heightNode = $xpath->query('//tei:supportDesc/tei:extent/tei:dimensions/tei:height');
        if (!empty($heightNode->item(0)->nodeValue)) {
            $height = $heightNode->item(0)->nodeValue;
        }

        $widthNode = $xpath->query('//tei:supportDesc/tei:extent/tei:dimensions/tei:width');
        if (!empty($widthNode->item(0)->nodeValue)) {
            $width = $widthNode->item(0)->nodeValue;
        }

        $unitNode = $xpath->query('//tei:supportDesc/tei:extent/tei:dimensions/@unit');
        if (!empty($unitNode->item(0)->nodeValue)) {
            $unit = $unitNode->item(0)->nodeValue;
        }

        if ((isset($height) && !empty($height)) &&
            (isset($width) && !empty($width)) &&
            (isset($unit) && !empty($unit))) {
            $scriptSource .= $height . ' x ' . $width . ' ' . $unit . '.';
        }

        $bindingDescNode = $xpath->query('//tei:bindingDesc/tei:p/text()');
        if (!empty($bindingDescNode->item(0)->data)) {
            $scriptSource .= ' ' . trim(preg_replace('/\s+/', ' ', $bindingDescNode->item(0)->data));
        }

        return $scriptSource;
    }

    public function getSettlement(DOMXPath $xpath): string
    {
        $settlement = '';
        $settlementNode = $xpath->query('//tei:settlement');
        if ($settlementNode->item(0)) {
            $settlement = $settlementNode->item(0)->nodeValue;
        }

        return $settlement;
    }

    public function getShelfmark(DOMXPath $xpath): string
    {
        $shelfmark = '';
        $shelfmarkNode = $xpath->query('//tei:idno');
        if ($shelfmarkNode->item(0)) {
            $shelfmark = trim(preg_replace('/\s+/', ' ', $shelfmarkNode->item(0)->nodeValue));
        }

        return $shelfmark;
    }

    public function getShortTitle(DOMXPath $xpath): string
    {
        $shortTitleNodeList = $xpath->query('//tei:title[@type = "short"]/descendant::text()');
        $shortTitle = '';
        foreach ($shortTitleNodeList as $k => $shortTitleNode) {
            $shortTitle .= $shortTitleNode->data;
        }

        $shortTitle = trim(preg_replace('/\s+/', ' ', $shortTitle));

        return $shortTitle;
    }

    public function getSourceDescription(DOMXPath $xpath): string
    {
        $sourceDescription = '';
        $sourceDescriptionNodeList = $xpath->query('//tei:sourceDesc/descendant::text()');
        foreach ($sourceDescriptionNodeList as $sourceDescriptionNode) {
            $sourceDescription .= $sourceDescriptionNode->data;
        }

        if (!empty($sourceDescription)) {
            $sourceDescription = trim(preg_replace('/\s+/', ' ', $sourceDescription));
        }

        return $sourceDescription;
    }

    public function getTitle(DOMXPath $xpath): string
    {
        $titleNodeList = $xpath->query('//tei:title[@type = "desc"]/descendant::text()');
        $title = '';
        foreach ($titleNodeList as $k => $titleNode) {
            $title .= $titleNode->data;
        }

        $title = trim(preg_replace('/\s+/', ' ', $title));

        return $title;
    }

    public function getWriters(DOMXPath $xpath): array
    {
        $writers = [];
        $writerNodes = $xpath->query('//tei:handNote');
        if (is_iterable($writerNodes)) {
            foreach ($writerNodes as $writerNode) {
                foreach ($writerNode->attributes as $attribute) {
                    if ('major' === $attribute->nodeValue) {
                        $textAddition = '– (Grundschicht)';
                    }
                }

                if (!empty($writerNode->textContent)) {
                    $writer = trim(preg_replace('/\s+/', ' ', $writerNode->textContent));
                    if (isset($textAddition)) {
                        $writer .= ' ' . $textAddition;
                    }
                }

                $writers[] = $writer;
                unset($textAddition);
            }
        }

        return $writers;
    }

    private function convertDate(string $dateString): string
    {
        if (4 === strlen($dateString)) {
            $dateString = "$dateString-01-01";
        }
        $datetime = new DateTime($dateString);

        return explode('+', $datetime->format(DateTime::ATOM))[0] . 'Z';
    }

    private function transformRelatedItems(DOMNodeList $nodes): array
    {
        $relatedItems = [];
        if (is_iterable($nodes)) {
            foreach ($nodes as $node) {
                if (!empty($node->nodeValue)) {
                    $text = trim(preg_replace('/\s+/', ' ', $node->nodeValue));
                    $refLink = $node->attributes->item(0)->textContent;
                    $documentId = array_reverse(explode('/', $refLink))[0];

                    if (str_contains($documentId, '.')) {
                        $documentId = explode('.', $documentId)[0];
                        $documentUrl = $this->mainDomain . $this->router->generate('_repository_detail', ['id' => $documentId]);
                    }

                    // Since the related items contain two pieces of data (url and label)
                    // we use the JSON string to store each item.
                    // The EMOBundle will decode it back and return objects instead of strings.

                    $relatedItems[] = json_encode([
                        'key' => $documentUrl ?? '',
                        'value' => $text,
                    ]);
                }
            }
        }

        return $relatedItems;
    }
}
