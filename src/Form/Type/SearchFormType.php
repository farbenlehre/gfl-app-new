<?php

namespace App\Form\Type;

use App\Model\Search;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('q', SearchType::class, ['attr' => ['class' => 'form-control mr-sm-2']])
            ->add('search', SubmitType::class, ['label' => 'Search', 'attr' => ['class' => 'btn btn-secondary my-2 my-sm-0']])
            ->setMethod(Request::METHOD_GET);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
             'data_class' => Search::class,
         ]);
    }
}
