<?php

namespace App\Command;

use App\Import\ImporterInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:images_to_s3')]
class ImagesToS3Storage extends Command
{
    private ImporterInterface $importer;

    public function __construct(ImporterInterface $importer)
    {
        parent::__construct();
        $this->importer = $importer;
    }

    protected function configure()
    {
        $this
            ->setDescription('Synchronize images in S3 with Owncloud.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Start importing images into S3 storage.');

        $message = $this->importer->importImagesToS3Storage();

        if (!empty($message)) {
            $output->writeln('IMPORT ERROR: '.$message);

            return Command::FAILURE;
        }

        $time = microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'];
        $time /= 60;
        $output->writeln('Import process completed in '.$time.' minutes.');

        return Command::SUCCESS;
    }
}
