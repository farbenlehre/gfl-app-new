<?php

declare(strict_types=1);

namespace App\Command;

use App\Import\ImporterInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:tei_to_s3')]
class TeiToS3Storage extends Command
{
    private ImporterInterface $importer;

    public function __construct(ImporterInterface $importer)
    {
        parent::__construct();
        $this->importer = $importer;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setDescription('Import TEI files from gitlab to S3 storage.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Start importing TEI files into S3 storage.');

        $this->importer->importTeiToS3Storage();

        $time = microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'];
        $time /= 60;
        $output->writeln('Import process completed in '.$time.' minutes.');

        return Command::SUCCESS;
    }
}
