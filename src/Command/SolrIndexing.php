<?php

declare(strict_types=1);

namespace App\Command;

use App\Import\ImporterInterface;
use App\Index\IndexerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'app:start_indexing')]
class SolrIndexing extends Command
{
    protected static string $description = 'Process TEI data to solr data for importing into solr.';
    private ImporterInterface $importer;
    private IndexerInterface $indexer;

    public function __construct(ImporterInterface $importer, IndexerInterface $indexer)
    {
        parent::__construct();
        $this->importer = $importer;
        $this->indexer = $indexer;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription(self::$description)
            ->addArgument('server', InputArgument::OPTIONAL, 'Server Type', 'dev');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $server = $input->getArgument('server');
        $output->writeln('Start solr indexing.');
        $this->importer->importTeisForIndexing($server);
        $this->indexer->deleteSolrIndex();
        $this->indexer->tei2Solr($server);
        $this->indexer->lit2Solr();
        $time = microtime(true) - $_SERVER['REQUEST_TIME_FLOAT'];
        $time /= 60;
        $output->writeln('Indexing process completed in '.$time.' minutes.');

        return Command::SUCCESS;
    }
}
