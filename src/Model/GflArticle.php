<?php
namespace App\Model;

use Subugoe\TextApiBundle\Model\ArticleInterface;

class GflArticle implements ArticleInterface
{
    private ?string $author = null;

    private string $content;

    private string $id;

    private ?string $imageUrl = null;

    private ?string $license = null;

    private array $metadata;

    private ?string $originDate = null;

    private ?string $originPlace = null;

    private ?string $title = null;

    private ?string $imageLicense = null;

    private ?string $imageLicenseUrl = null;

    private array $allAnnotationIds;
    private string $editedText;
    private array $gndsUuids;
    private array $pageLevelEditedText;
    private array $pageLevelTranscriptedText;
    private array $pagesDates;
    private array $pagesEntities;
    private array $pagesEntitiesTypes;
    private array $pagesFoliants;
    private array $pagesGndsUuids;
    private array $pagesLiterature;
    private array $pagesNotes;
    private array $pagesWorks;
    private string $transcriptedText;

    private ?string $recipient = null;

    private ?array $pageDates = null;

    private ?array $pageDatesIds = null;

    private ?array $pageNotes = null;

    private ?array $pageNotesIds = null;

    private ?array $pageWorks = null;

    private ?array $pageWorksIds = null;

    private ?array $pageEntities = null;

    private ?array $pageEntitiesIds = null;

    private ?array $pageEntitiesTypes = null;

    private ?array $pageNotesAbstracts = null;

    private ?array $pageNotesAbstractsIds = null;

    private ?array $pageAllAnnotationIds = null;

    private ?string $pageNumber = null;

    private ?string $pageFoliant = null;

    private ?string $publishDate = null;

    private ?array $references = null;

    private ?array $related_items = null;

    private ?array $responses = null;

    private ?string $script_source = null;

    private ?string $shelfmark = null;

    private ?array $writer = null;

    private ?string $leopoldinaEdition = null;

    private ?string $printSource = null;

    private ?string $destinationPlace = null;

    private ?array $pageSegs = null;

    private ?array $entities = null;

    private ?array $language = null;

    private ?array $articleTitle = null;

    private ?array $gndKeywords = null;

    private ?array $freeKeywords = null;

    private ?string $institution = null;

    private array $pageIds = [];

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(?string $author): void
    {
        $this->author = $author;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    public function setImageUrl(?string $imageUrl): void
    {
        $this->imageUrl = $imageUrl;
    }

    public function getLicense(): ?string
    {
        return $this->license;
    }

    public function setLicense(?string $license): void
    {
        $this->license = $license;
    }

    public function getMetadata(): array
    {
        return $this->metadata;
    }

    public function setMetadata(array $metadata): void
    {
        $this->metadata = $metadata;
    }

    public function getOriginDate(): ?string
    {
        return $this->originDate;
    }

    public function setOriginDate(?string $originDate): void
    {
        $this->originDate = $originDate;
    }

    public function getOriginPlace(): ?string
    {
        return $this->originPlace;
    }

    public function setOriginPlace(?string $originPlace): void
    {
        $this->originPlace = $originPlace;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    public function getImageLicense(): ?string
    {
        return $this->imageLicense;
    }

    public function setImageLicense(?string $imageLicense): void
    {
        $this->imageLicense = $imageLicense;
    }

    public function getImageLicenseLink(): ?string
    {
        return $this->imageLicenseUrl;
    }

    public function setImageLicenseLink(?string $imageLicenseUrl): void
    {
        $this->imageLicenseUrl = $imageLicenseUrl;
    }

    public function getAllAnnotationIds(): array
    {
        return $this->allAnnotationIds;
    }

    public function setAllAnnotationIds(array $allAnnotationIds): void
    {
        $this->allAnnotationIds = $allAnnotationIds;
    }

    public function getEditedText(): string
    {
        return $this->editedText;
    }

    public function setEditedText(string $editedText): void
    {
        $this->editedText = $editedText;
    }

    public function getGndsUuids(): array
    {
        return $this->gndsUuids;
    }

    public function setGndsUuids(array $gndsUuids): void
    {
        $this->gndsUuids = $gndsUuids;
    }

    public function getPageLevelEditedText(): array
    {
        return $this->pageLevelEditedText;
    }

    public function setPageLevelEditedText(array $pageLevelEditedText): void
    {
        $this->pageLevelEditedText = $pageLevelEditedText;
    }

    public function getPageLevelTranscriptedText(): array
    {
        return $this->pageLevelTranscriptedText;
    }

    public function setPageLevelTranscriptedText(array $pageLevelTranscriptedText): void
    {
        $this->pageLevelTranscriptedText = $pageLevelTranscriptedText;
    }

    public function getPagesDates(): array
    {
        return $this->pagesDates;
    }

    public function setPagesDates(array $pagesDates): void
    {
        $this->pagesDates = $pagesDates;
    }

    public function getPagesEntities(): array
    {
        return $this->pagesEntities;
    }

    public function setPagesEntities(array $pagesEntities): void
    {
        $this->pagesEntities = $pagesEntities;
    }

    public function getPagesEntitiesTypes(): array
    {
        return $this->pagesEntitiesTypes;
    }

    public function setPagesEntitiesTypes(array $pagesEntitiesTypes): void
    {
        $this->pagesEntitiesTypes = $pagesEntitiesTypes;
    }

    public function getPagesFoliants(): array
    {
        return $this->pagesFoliants;
    }

    public function setPagesFoliants(array $pagesFoliants): void
    {
        $this->pagesFoliants = $pagesFoliants;
    }

    public function getPagesGndsUuids(): array
    {
        return $this->pagesGndsUuids;
    }

    public function setPagesGndsUuids(array $pagesGndsUuids): void
    {
        $this->pagesGndsUuids = $pagesGndsUuids;
    }

    public function getPagesLiterature(): array
    {
        return $this->pagesLiterature;
    }

    public function setPagesLiterature(array $pagesLiterature): void
    {
        $this->pagesLiterature = $pagesLiterature;
    }

    public function getPagesNotes(): array
    {
        return $this->pagesNotes;
    }

    public function setPagesNotes(array $pagesNotes): void
    {
        $this->pagesNotes = $pagesNotes;
    }

    public function getPagesWorks(): array
    {
        return $this->pagesWorks;
    }

    public function setPagesWorks(array $pagesWorks): void
    {
        $this->pagesWorks = $pagesWorks;
    }

    public function getTranscriptedText(): string
    {
        return $this->transcriptedText;
    }

    public function setTranscriptedText(string $transcriptedText): void
    {
        $this->transcriptedText = $transcriptedText;
    }

    public function getRecipient(): ?string
    {
        return $this->recipient;
    }

    public function setRecipient(?string $recipient): void
    {
        $this->recipient = $recipient;
    }

    public function getPageDates(): ?array
    {
        return $this->pageDates;
    }

    public function setPageDates(?array $pageDates): void
    {
        $this->pageDates = $pageDates;
    }

    public function getPageDatesIds(): ?array
    {
        return $this->pageDatesIds;
    }

    public function setPageDatesIds(?array $pageDatesIds): void
    {
        $this->pageDatesIds = $pageDatesIds;
    }

    public function getPageNotes(): ?array
    {
        return $this->pageNotes;
    }

    public function setPageNotes(?array $pageNotes): void
    {
        $this->pageNotes = $pageNotes;
    }

    public function getPageNotesIds(): ?array
    {
        return $this->pageNotesIds;
    }

    public function setPageNotesIds(?array $pageNotesIds): void
    {
        $this->pageNotesIds = $pageNotesIds;
    }

    public function getPageWorks(): ?array
    {
        return $this->pageWorks;
    }

    public function setPageWorks(?array $pageWorks): void
    {
        $this->pageWorks = $pageWorks;
    }

    public function getPageWorksIds(): ?array
    {
        return $this->pageWorksIds;
    }

    public function setPageWorksIds(?array $pageWorksIds): void
    {
        $this->pageWorksIds = $pageWorksIds;
    }

    public function getPageEntities(): ?array
    {
        return $this->pageEntities;
    }

    public function setPageEntities(?array $pageEntities): void
    {
        $this->pageEntities = $pageEntities;
    }

    public function getPageEntitiesIds(): ?array
    {
        return $this->pageEntitiesIds;
    }

    public function setPageEntitiesIds(?array $pageEntitiesIds): void
    {
        $this->pageEntitiesIds = $pageEntitiesIds;
    }

    public function getPageEntitiesTypes(): ?array
    {
        return $this->pageEntitiesTypes;
    }

    public function setPageEntitiesTypes(?array $pageEntitiesTypes): void
    {
        $this->pageEntitiesTypes = $pageEntitiesTypes;
    }

    public function getPageNotesAbstracts(): ?array
    {
        return $this->pageNotesAbstracts;
    }

    public function setPageNotesAbstracts(?array $pageNotesAbstracts): void
    {
        $this->pageNotesAbstracts = $pageNotesAbstracts;
    }

    public function getPageNotesAbstractsIds(): ?array
    {
        return $this->pageNotesAbstractsIds;
    }

    public function setPageNotesAbstractsIds(?array $pageNotesAbstractsIds): void
    {
        $this->pageNotesAbstractsIds = $pageNotesAbstractsIds;
    }

    public function getPageAllAnnotationIds(): ?array
    {
        return $this->pageAllAnnotationIds;
    }

    public function setPageAllAnnotationIds(?array $pageAllAnnotationIds): void
    {
        $this->pageAllAnnotationIds = $pageAllAnnotationIds;
    }

    public function getPageNumber(): ?string
    {
        return $this->pageNumber;
    }

    public function setPageNumber(?string $pageNumber): void
    {
        $this->pageNumber = $pageNumber;
    }

    public function getPageFoliant(): ?string
    {
        return $this->pageFoliant;
    }

    public function setPageFoliant(?string $pageFoliant): void
    {
        $this->pageFoliant = $pageFoliant;
    }

    public function getPublishDate(): ?string
    {
        return $this->publishDate;
    }

    public function setPublishDate(?string $publishDate): void
    {
        $this->publishDate = $publishDate;
    }

    public function getReferences(): ?array
    {
        return $this->references;
    }

    public function setReferences(?array $references): void
    {
        $this->references = $references;
    }

    public function getRelatedItems(): ?array
    {
        return $this->related_items;
    }

    public function setRelatedItems(?array $related_items): void
    {
        $this->related_items = $related_items;
    }

    public function getResponses(): ?array
    {
        return $this->responses;
    }

    public function setResponses(?array $responses): void
    {
        $this->responses = $responses;
    }

    public function getScriptSource(): ?string
    {
        return $this->script_source;
    }

    public function setScriptSource(?string $script_source): void
    {
        $this->script_source = $script_source;
    }

    public function getShelfmark(): ?string
    {
        return $this->shelfmark;
    }

    public function setShelfmark(?string $shelfmark): void
    {
        $this->shelfmark = $shelfmark;
    }

    public function getWriter(): ?array
    {
        return $this->writer;
    }

    public function setWriter(?array $writer): void
    {
        $this->writer = $writer;
    }

    public function getLeopoldinaEdition(): ?string
    {
        return $this->leopoldinaEdition;
    }

    public function setLeopoldinaEdition(?string $leopoldinaEdition): void
    {
        $this->leopoldinaEdition = $leopoldinaEdition;
    }

    public function getPrintSource(): ?string
    {
        return $this->printSource;
    }

    public function setPrintSource(?string $printSource): void
    {
        $this->printSource = $printSource;
    }

    public function getDestinationPlace(): ?string
    {
        return $this->destinationPlace;
    }

    public function setDestinationPlace(?string $destinationPlace): void
    {
        $this->destinationPlace = $destinationPlace;
    }

    public function getPageSegs(): ?array
    {
        return $this->pageSegs;
    }

    public function setPageSegs(?array $pageSegs): void
    {
        $this->pageSegs = $pageSegs;
    }

    public function getEntities(): ?array
    {
        return $this->entities;
    }

    public function setEntities(?array $entities): void
    {
        $this->entities = $entities;
    }

    public function getLanguage(): ?array
    {
        return $this->language;
    }

    public function setLanguage(?array $language): void
    {
        $this->language = $language;
    }

    public function getArticleTitle(): ?array
    {
        return $this->articleTitle;
    }

    public function setArticleTitle(?array $articleTitle): void
    {
        $this->articleTitle = $articleTitle;
    }

    public function getGndKeywords(): ?array
    {
        return $this->gndKeywords;
    }

    public function setGndKeywords(?array $gndKeywords): void
    {
        $this->gndKeywords = $gndKeywords;
    }

    public function getFreeKeywords(): ?array
    {
        return $this->freeKeywords;
    }

    public function setFreeKeywords(?array $freeKeywords): void
    {
        $this->freeKeywords = $freeKeywords;
    }

    public function getInstitution(): ?string
    {
        return $this->institution;
    }

    public function setInstitution(?string $institution): void
    {
        $this->institution = $institution;
    }

    public function getImageLicenseUrl(): ?string
    {
        return $this->imageLicenseUrl;
    }

    public function setImageLicenseUrl(?string $imageLicenseUrl): void
    {
        $this->imageLicenseUrl = $imageLicenseUrl;
    }

    public function getPageIds(): array
    {
        return $this->pageIds;
    }

    public function setPageIds(array $pageIds): void
    {
        $this->pageIds = $pageIds;
    }

}

