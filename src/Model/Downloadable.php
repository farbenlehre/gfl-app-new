<?php

namespace App\Model;

class Downloadable
{
    private ?string $name;
    private ?string $url;
    private ?string $title;
    private int $size = 0;

    /**
     * @param string|null $name
     * @param string|null $url
     * @param int $size
     */
    public function __construct(?string $name, ?string $url, ?string $title, int $size)
    {
        $this->name = $name;
        $this->url = $url;
        $this->size = $size;
        $this->title = $title;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): void
    {
        $this->url = $url;
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function setSize(int $size): void
    {
        $this->size = $size;
    }

    public function getSizeFormatted(): string
    {
        return $this->formatBytes($this->size);
    }

    public function formatBytes($bytes, $precision = 2): string
    {
        $units = array('B', 'KiB', 'MiB', 'GiB', 'TiB');
        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        // Uncomment one of the following alternatives
        $bytes /= pow(1024, $pow);

        return round($bytes, $precision) . ' ' . $units[$pow];
    }
}
