<?php

namespace App\Model;

use JetBrains\PhpStorm\Pure;
use Subugoe\TextApiBundle\Model\PageInterface;

class GflPage implements PageInterface
{
    private ?string $articleId = null;

    private ?string $articleTitle = null;

    private string $id;

    private ?string $imageUrl = null;

    private ?string $title = null;

    private ?string $imageLicense = null;

    private ?string $imageLicenseUrl = null;

    private ?string $language = null;

    private ?string $enumeration = null;

    private array $texts = [];

    private ?string $transcriptedText = null;

    private ?string $editedText = null;

    private array $contentTypes = ['transcription', 'edited'];

    private ?array $languages = null;

    private ?int $pageNumber = null;

    private ?string $annotationCollectionLabel = null;

    private ?array $pageDates = null;

    private ?array $pageDatesIds = null;

    private ?array $pageNotes = null;

    private ?array $pageNotesIds = null;

    private ?array $pageWorks = null;

    private ?array $pageWorksIds = null;

    private ?array $pageEntities = null;

    private ?array $pageEntitiesIds = null;

    private ?array $pageEntitiesTypes = null;

    private ?array $pageNotesAbstracts = null;

    private ?array $pageNotesAbstractsIds = null;

    private ?array $pageAllAnnotationIds = null;

    private ?array $pageSegs = null;

    public function getArticleId(): ?string
    {
        return $this->articleId;
    }

    public function setArticleId(?string $articleId): void
    {
        $this->articleId = $articleId;
    }

    public function getArticleTitle(): ?string
    {
        return $this->articleTitle;
    }

    public function setArticleTitle(?string $articleTitle): void
    {
        $this->articleTitle = $articleTitle;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function getImageUrl(): ?string
    {
        return $this->imageUrl;
    }

    public function setImageUrl(?string $imageUrl): void
    {
        $this->imageUrl = $imageUrl;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    public function getImageLicense(): ?string
    {
        return $this->imageLicense;
    }

    public function setImageLicense(?string $imageLicense): void
    {
        $this->imageLicense = $imageLicense;
    }

    public function getImageLicenseUrl(): ?string
    {
        return $this->imageLicenseUrl;
    }

    public function setImageLicenseUrl(?string $imageLicenseUrl): void
    {
        $this->imageLicenseUrl = $imageLicenseUrl;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(?string $language): void
    {
        $this->language = $language;
    }

    public function getEnumeration(): ?string
    {
        return $this->enumeration;
    }

    public function setEnumeration(?string $enumeration): void
    {
        $this->enumeration = $enumeration;
    }

    public function getTexts(): array
    {
        return $this->texts;
    }

    public function setTexts(array $texts): void
    {
        $this->texts = $texts;
    }

    public function getTranscriptedText(): ?string
    {
        return $this->transcriptedText;
    }

    public function setTranscriptedText(?string $transcriptedText): void
    {
        $this->transcriptedText = $transcriptedText;
    }

    public function getEditedText(): ?string
    {
        return $this->editedText;
    }

    public function setEditedText(?string $editedText): void
    {
        $this->editedText = $editedText;
    }

    public function getContentTypes(): array
    {
        return $this->contentTypes;
    }

    public function setContentTypes(array $contentTypes): void
    {
        $this->contentTypes = $contentTypes;
    }

    public function getLanguages(): ?array
    {
        return $this->languages;
    }

    public function setLanguages(?array $languages): void
    {
        $this->languages = $languages;
    }

    #[Pure] public function getContentByType(string $type): ?string
    {
        if ($type === 'transcription') return $this->getTranscriptedText();
        else if ($type === 'edited') return $this->getEditedText();

        return null;
    }

    public function getPageNumber(): ?int
    {
        return $this->pageNumber;
    }

    public function setPageNumber(?int $pageNumber): void
    {
        $this->pageNumber = $pageNumber;
    }

    public function getAnnotationCollectionLabel(): ?string
    {
        return $this->annotationCollectionLabel;
    }

    public function setAnnotationCollectionLabel(?string $annotationCollectionLabel): void
    {
        $this->annotationCollectionLabel = $annotationCollectionLabel;
    }

    public function getPageDates(): ?array
    {
        return $this->pageDates;
    }

    public function setPageDates(?array $pageDates): void
    {
        $this->pageDates = $pageDates;
    }

    public function getPageDatesIds(): ?array
    {
        return $this->pageDatesIds;
    }

    public function setPageDatesIds(?array $pageDatesIds): void
    {
        $this->pageDatesIds = $pageDatesIds;
    }

    public function getPageNotes(): ?array
    {
        return $this->pageNotes;
    }

    public function setPageNotes(?array $pageNotes): void
    {
        $this->pageNotes = $pageNotes;
    }

    public function getPageNotesIds(): ?array
    {
        return $this->pageNotesIds;
    }

    public function setPageNotesIds(?array $pageNotesIds): void
    {
        $this->pageNotesIds = $pageNotesIds;
    }

    public function getPageWorks(): ?array
    {
        return $this->pageWorks;
    }

    public function setPageWorks(?array $pageWorks): void
    {
        $this->pageWorks = $pageWorks;
    }

    public function getPageWorksIds(): ?array
    {
        return $this->pageWorksIds;
    }

    public function setPageWorksIds(?array $pageWorksIds): void
    {
        $this->pageWorksIds = $pageWorksIds;
    }

    public function getPageEntities(): ?array
    {
        return $this->pageEntities;
    }

    public function setPageEntities(?array $pageEntities): void
    {
        $this->pageEntities = $pageEntities;
    }

    public function getPageEntitiesIds(): ?array
    {
        return $this->pageEntitiesIds;
    }

    public function setPageEntitiesIds(?array $pageEntitiesIds): void
    {
        $this->pageEntitiesIds = $pageEntitiesIds;
    }

    public function getPageEntitiesTypes(): ?array
    {
        return $this->pageEntitiesTypes;
    }

    public function setPageEntitiesTypes(?array $pageEntitiesTypes): void
    {
        $this->pageEntitiesTypes = $pageEntitiesTypes;
    }

    public function getPageNotesAbstracts(): ?array
    {
        return $this->pageNotesAbstracts;
    }

    public function setPageNotesAbstracts(?array $pageNotesAbstracts): void
    {
        $this->pageNotesAbstracts = $pageNotesAbstracts;
    }

    public function getPageNotesAbstractsIds(): ?array
    {
        return $this->pageNotesAbstractsIds;
    }

    public function setPageNotesAbstractsIds(?array $pageNotesAbstractsIds): void
    {
        $this->pageNotesAbstractsIds = $pageNotesAbstractsIds;
    }

    public function getPageAllAnnotationIds(): ?array
    {
        return $this->pageAllAnnotationIds;
    }

    public function setPageAllAnnotationIds(?array $pageAllAnnotationIds): void
    {
        $this->pageAllAnnotationIds = $pageAllAnnotationIds;
    }

    public function getPageSegs(): ?array
    {
        return $this->pageSegs;
    }

    public function setPageSegs(?array $pageSegs): void
    {
        $this->pageSegs = $pageSegs;
    }
}

