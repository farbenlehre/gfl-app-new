<?php

namespace App\Index;

use App\Model\SolrDocument;

interface IndexerInterface
{
    public function deleteSolrIndex(): void;

    public function getTextVersions(string $filePath, array $graphics = []): SolrDocument;

    public function tei2solr(string $server): void;
}
