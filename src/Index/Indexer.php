<?php

declare(strict_types=1);

namespace App\Index;

use App\Import\HTMLDocument;
use App\Model\SolrDocument;
use App\Service\PreProcessingService;
use App\Transform\EditedTextTransformer;
use App\Transform\MetadataTransformerInterface;
use App\Transform\TranscriptionTransformer;
use DOMDocument;
use DOMElement;
use DOMXPath;
use Exception;
use Solarium\Client;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class Indexer implements IndexerInterface
{
    private const ARTICLE_DOC_TYPE = 'article';
    private const ENTITY_DOC_TYPE = 'entity';
    private const NOTE_DOC_TYPE = 'note';
    private const PAGE_DOC_TYPE = 'page';
    private Client $client;
    private EditedTextTransformer $editedTextTransformer;
    private array $imageLicenses;
    private ?string $litDir;
    private ?array $literaturDataElements;
    private MetadataTransformerInterface $metadataTransformer;
    private PreProcessingService $preProcessingService;
    private ?string $teiDir = null;
    private ?string $teiSampleDir = null;
    private TranscriptionTransformer $transcriptionTransformer;

    public function __construct(
        Client $client,
        PreProcessingService $preProcessingService,
        TranscriptionTransformer $transcriptionTransformer,
        EditedTextTransformer $editedTextTransformer,
        MetadataTransformerInterface $metadataTransformer,
        ParameterBagInterface $params
    ) {
        $this->client = $client;
        $this->transcriptionTransformer = $transcriptionTransformer;
        $this->editedTextTransformer = $editedTextTransformer;
        $this->preProcessingService = $preProcessingService;
        $this->metadataTransformer = $metadataTransformer;
        $this->setConfigs(
            $params->get('tei_dir'),
            $params->get('tei_sample_dir'),
            $params->get('lit_dir'),
            $params->get('literatur_data_elements'),
            $params->get('image_licenses'),
        );
    }

    public function deleteSolrIndex(): void
    {
        $update = $this->client->createUpdate();
        $update->addDeleteQuery('*:*');
        $update->addCommit();
        $this->client->execute($update);
    }

    public function getTextVersions(string $filePath, array $graphics = [], array $entitiesMap = []): SolrDocument
    {
        $doc = new DOMDocument();
        $doc->load($filePath, LIBXML_NOBLANKS);

        $pageLevelEditedText = [];
        $pageLevelTranscriptedText = [];
        $pagesGndsUuids = [];
        $pagesNotes = [];
        $pagesDates = [];
        $pagesWorks = [];
        $pagesEntities = [];
        $pagesEntitiesTypes = [];
        $pagesAllAnnotationIds = [];
        $pagesLiterature = [];
        $pagesFoliants = [];

        // Handle transcription text transformation
        // ========================================================
        $transcriptionXPath = new DOMXPath($doc);
        $transcriptionXPath->registerNamespace('tei', 'http://www.tei-c.org/ns/1.0');

        $transcriptionPageNodes = $transcriptionXPath->query('//tei:body');

        /** @var DOMElement $body */
        $transcriptionBody = $transcriptionPageNodes[0];
        $this->transcriptionTransformer->setGraphics($graphics);

        $transcriptionPages = $this->preProcessingService->splitByPages($transcriptionBody);
        $this->preProcessingService->clear();

        foreach ($transcriptionPages as $key => $page) {
            if ($key > 0) {
                $transcriptedDoc = $this->transcriptionTransformer->transformPage($page);
                $pageLevelTranscriptedText[] = htmlspecialchars_decode($transcriptedDoc->saveHTML());
            }
        }

        $documentLevelTranscriptedText = '';

        foreach ($pageLevelTranscriptedText as $singlePageTranscriptedText) {
            $documentLevelTranscriptedText .= $singlePageTranscriptedText;
        }

        // Handle edited text transformation
        // ========================================================

        $editedTextXPath = new DOMXPath($doc);
        $editedTextXPath->registerNamespace('tei', 'http://www.tei-c.org/ns/1.0');

        // $editedTextXPath = $this->preProcessingService->removeHyphens($editedTextXPath);

        $editedTextPageNodes = $editedTextXPath->query('//tei:body');

        /** @var DOMElement $body */
        $editedTextBody = $editedTextPageNodes[0];
        $this->editedTextTransformer->setGraphics($graphics);
        $this->editedTextTransformer->setEntitiesMap($entitiesMap);

        $editedTextPages = $this->preProcessingService->splitByPages($editedTextBody);
        $this->preProcessingService->clear();

        foreach ($editedTextPages as $key => $page) {
            if ($key > 0) {
                $pageIndex = $key - 1;
                $editedDoc = $this->editedTextTransformer->transformPage($page);
                $pagesGndsUuids[$pageIndex] = $this->editedTextTransformer->getGndsUuids();
                $pagesNotes[$pageIndex] = $this->editedTextTransformer->getNotes();
                $pagesDates[$pageIndex] = $this->editedTextTransformer->getDates();
                $pagesWorks[$pageIndex] = $this->editedTextTransformer->getWorks();
                $pagesEntities[$pageIndex] = $this->editedTextTransformer->getEntities();
                $pagesEntitiesTypes[$pageIndex] = $this->editedTextTransformer->getEntitiesTypes();
                $pagesAllAnnotationIds[$pageIndex] = $this->editedTextTransformer->getAllAnnotationIds();
                $pagesLiterature[$pageIndex] = $this->editedTextTransformer->getLiterature();
                $pagesFoliants[$pageIndex] = $this->editedTextTransformer->getFoliant();
                $pageLevelEditedText[] = $this->handlePlaceholders(htmlspecialchars_decode($editedDoc->saveHTML()));
                $this->editedTextTransformer->clear();
            }
        }

        $gndsUuids = [...$pagesGndsUuids];

        $documentLevelEditedText = '';

        foreach ($pageLevelEditedText as $singlePageEditedText) {
            $documentLevelEditedText .= $singlePageEditedText;
        }

        $solrDocument = new SolrDocument();
        $solrDocument->setTranscriptedText($documentLevelTranscriptedText);
        $solrDocument->setPageLevelTranscriptedText($pageLevelTranscriptedText);
        $solrDocument->setEditedText($documentLevelEditedText);
        $solrDocument->setPageLevelEditedText($pageLevelEditedText);
        $solrDocument->setGndsUuids($gndsUuids);
        $solrDocument->setPagesGndsUuids($pagesGndsUuids);
        $solrDocument->setPagesNotes($pagesNotes);
        $solrDocument->setPagesDates($pagesDates);
        $solrDocument->setPagesWorks($pagesWorks);
        $solrDocument->setPagesEntities($pagesEntities);
        $solrDocument->setPagesEntitiesTypes($pagesEntitiesTypes);
        $solrDocument->setPagesLiterature($pagesLiterature);
        $solrDocument->setAllAnnotationIds($pagesAllAnnotationIds);
        $solrDocument->setPagesFoliants($pagesFoliants);

        return $solrDocument;
    }

    public function handlePlaceholders($str): string
    {
        return str_replace(['-@@nobreak@@', '@@nobreak@@'], '', $str);
    }

    public function lit2Solr(): void
    {
        $this->client->getEndpoint()->setOptions(['timeout' => 60, 'index_timeout' => 60]);
        $finder = new Finder();
        $finder->files()->in($this->litDir);
        foreach ($finder as $file) {
            libxml_use_internal_errors(true);
            $doc = new \DOMDocument();
            $doc->load($file->getRealPath());
            if (!libxml_get_errors()) {
                $xpath = new \DOMXPath($doc);
                $xpath->registerNamespace('tei', 'http://www.tei-c.org/ns/1.0');
                $literature = $xpath->query('//tei:text//tei:body//tei:listBibl//tei:bibl');

                foreach ($literature as $literatureItem) {
                    $update = $this->client->createUpdate();
                    $sdoc = $update->createDocument();

                    $xmlString = $doc->saveXML($literatureItem);

                    $uri = [];
                    $author = [];
                    $publisher = [];
                    $pubPlace = [];
                    $edition = [];

                    if (!empty($xmlString)) {
                        $sdoc->xml = $xmlString;
                    }

                    foreach ($literatureItem->childNodes as $childNode) {
                        /** @var DOMElement $childNode */
                        $name = str_replace('_', ' ', $literatureItem->attributes->item(0)->textContent);
                        $sdoc->id = $literatureItem->attributes->item(0)->textContent;
                        $sdoc->doctype = 'literature';
                        $sdoc->name = $name;

                        if ('#text' !== $childNode->nodeName) {
                            $text = trim(preg_replace('/\s+/', ' ', $childNode->nodeValue));

                            if ('relatedItem' === $childNode->nodeName) {
                                foreach ($childNode->childNodes as $childChildNode) {
                                    if ('ref' === $childChildNode->nodeName) {
                                        $ref = $childChildNode->getAttribute('target');
                                        if ('_' !== $ref) {
                                            $uri[] = $ref;
                                        }
                                    }
                                }
                            } elseif ('title' === $childNode->nodeName) {
                                if (!$childNode->hasAttributes()) continue;
                                $name = '';
                                    'title_'
                                    . $childNode->getAttribute('level')
                                    . '_'
                                    . $childNode->getAttribute('type');

                                if (!array_key_exists($name, $this->literaturDataElements)) continue;

                                $name = $this->literaturDataElements[$name];

                                if (!empty($name)) {
                                    $sdoc->$name = $text;
                                }
                            } elseif ('author' === $childNode->nodeName) {
                                foreach ($childNode->childNodes as $item) {
                                    $authorElement = trim(preg_replace('/\s+/', ' ', $item->nodeValue));

                                    if (!empty($authorElement)) {
                                        $author[] = $authorElement;
                                    }
                                }
                            } elseif ('publisher' === $childNode->nodeName) {
                                foreach ($childNode->childNodes as $item) {
                                    $publisherElement = trim(preg_replace('/\s+/', ' ', $item->nodeValue));

                                    if (!empty($publisherElement)) {
                                        $publisher[] = $publisherElement;
                                    }
                                }
                            } elseif ('pubPlace' === $childNode->nodeName) {
                                foreach ($childNode->childNodes as $item) {
                                    $pubPlaceElement = trim(preg_replace('/\s+/', ' ', $item->nodeValue));

                                    if (!empty($pubPlaceElement)) {
                                        $pubPlace[] = $pubPlaceElement;
                                    }
                                }
                            } elseif ('edition' === $childNode->nodeName) {
                                foreach ($childNode->childNodes as $item) {
                                    $editionElement = trim(preg_replace('/\s+/', ' ', $item->nodeValue));

                                    if (!empty($editionElement)) {
                                        $edition[] = $editionElement;
                                    }
                                }
                            } elseif ('idno' === $childNode->nodeName) {
                                $name = 'idno_'.strtolower($childNode->attributes->item(0)->nodeValue);
                                if (!array_key_exists($name, $this->literaturDataElements)) continue;

                                $name = $this->literaturDataElements[$name];
                                if (!empty($name) && !empty($text)) {
                                    $sdoc->$name = $text;
                                }
                            } elseif ('biblScope' === $childNode->nodeName) {
                                $name = 'biblScope_'.$childNode->attributes->item(0)->nodeValue;

                                if ($childNode->attributes->item(1) && 'n' === $childNode->attributes->item(1)->nodeName) {
                                    $name .= '_'.$childNode->attributes->item(1)->nodeValue;
                                }

                              if (!array_key_exists($name, $this->literaturDataElements)) continue;
                              $name = $this->literaturDataElements[$name];

                                if (!empty($name) && !empty($text)) {
                                    $sdoc->$name = $text;
                                }
                            } elseif ('note' === $childNode->nodeName) {
                                if ($childNode->hasAttribute('type')
                                    && 'citation-string' === $childNode->getAttribute('type')) {
                                    $name = 'citation_string';
                                }

                              if (!array_key_exists($name, $this->literaturDataElements)) continue;

                              $fieldName = $this->literaturDataElements[$name];
                              if (!empty($fieldName) && !empty($text)) {
                                    $sdoc->$fieldName = $text;
                                }
                            } else {
                                $name = strval($childNode->nodeName);

                                if (!array_key_exists($name, $this->literaturDataElements)) continue;
                                $name = $this->literaturDataElements[$name];
                                if (!empty($name) && !empty($text)) {
                                  $sdoc->$name = $text;
                                }

                            }
                        }

                        unset($text);
                        unset($name);
                    }

                    if ([] !== $uri) {
                        $sdoc->uri = $uri;
                    }

                    if ([] !== $author) {
                        $sdoc->literature_author = $author;
                    }

                    if ([] !== $publisher) {
                        $sdoc->publisher = $publisher;
                    }

                    if ([] !== $pubPlace) {
                        $sdoc->pub_place = $pubPlace;
                    }

                    if ([] !== $edition) {
                        $sdoc->edition = $edition;
                    }

                    $update->addDocument($sdoc);
                    $update->addCommit();
                    $this->client->execute($update);
                }
            }
        }
    }

    public function setConfigs(string $teiDir, string $teiSampleDir, string $litDir, array $literaturDataElements, array $imageLicenses): void
    {
        $this->teiDir = $teiDir;
        $this->teiSampleDir = $teiSampleDir;
        $this->litDir = $litDir;
        $this->literaturDataElements = $literaturDataElements;
        $this->imageLicenses = $imageLicenses;
    }

    public function tei2Solr(string $server, string $filename = null): void
    {
        $this->client->getEndpoint()->setOptions(['timeout' => 60, 'index_timeout' => 60]);
        $finder = new Finder();
        if ($filename != NULL) {
            $finder->files()->in($this->teiDir)->name($filename);
        } else {
            $finder->files()->in($this->teiDir);
        }

        if ('dev' === $server) {
            // ===========================================================
            // Append a sample file which is useful during development.
            // Remove for production
            $finderSample = new Finder();
            $finderSample->files()->in($this->teiSampleDir);
            $finder->append($finderSample);
            // ===========================================================
        }

        foreach ($finder as $file) {
            libxml_use_internal_errors(true);
            $doc = new \DOMDocument();
            $doc->load($file->getRealPath());

            if (!libxml_get_errors() and $doc == $filename) {
                $xpath = new \DOMXPath($doc);
                $xpath->registerNamespace('tei', 'http://www.tei-c.org/ns/1.0');
                $id = $this->getId($xpath);

                echo "Indexing $id...\n";

                $entities = $this->getEntities($xpath);

                $doctypeNotes = $this->getDoctypeNotes($xpath, $id);

                $this->indexNotes($doctypeNotes);

                $indexedEntities = $this->indexEntities($entities);

                $indexedEntitiesMap = [];

                foreach ($indexedEntities as $indexedEntity) {
                    $indexedEntitiesMap[$indexedEntity->id] = $indexedEntity;
                }

                $this->indexDocument($doc, $file->getRealPath(), $indexedEntitiesMap);
            } else {
                $filesystem = new Filesystem();
                $teiImportLogFile = './data/log/teiImportLogs.txt';

                if (!$filesystem->exists($teiImportLogFile)) {
                    $filesystem->mkdir('./data/log');
                    $filesystem->touch($teiImportLogFile);
                }

                $errors = [];
                foreach (libxml_get_errors() as $key => $error) {
                    if (0 === $key) {
                        $file = $error->file;
                        echo "Error in $file\n";
                        $errors[] = explode('/', $file)[4].PHP_EOL;
                        $errors[] = '--------------------'.PHP_EOL;
                    }

                    $errors[] = $error->message;
                }

                $filesystem->appendToFile($teiImportLogFile, implode('', $errors));

                libxml_clear_errors();
            }
        }

//        try {
//            $filesystem = new Filesystem();
//            $filesystem->remove($this->teiDir);
//        } catch (IOExceptionInterface $exception) {
//            echo 'Error deleting directory at'.$exception->getPath();
//        }
    }

    private function getAbstracts(DOMXPath $xpath): array
    {
        $abstracts = [];
        $abstractNodes = $xpath->query('//tei:abstract');
        foreach ($abstractNodes as $abstractNode) {
            $doc = new HTMLDocument();
            $transformed = $this->editedTextTransformer->transformElement($abstractNode, $doc);
            if ($transformed) {
                $doc->appendChild($transformed);
            }

            $abstracts[] = htmlspecialchars_decode($doc->saveHTML());
        }

        return $abstracts;
    }

    private function getDoctypeNotes(DOMXPath $xpath, string $id): array
    {
        $doctypeNotes = [];
        $notesNodes = $xpath->query('//tei:text[@xml:lang="ger"]//tei:div//tei:div//tei:note');
        if (is_iterable($notesNodes) && !empty($notesNodes)) {
            $notes = [];
            foreach ($notesNodes as $key => $notesNode) {
                if (!empty($notesNodes->item(0)->nodeValue) && !empty($id)) {
                    $note = trim(preg_replace('/\s+/', ' ', $notesNode->nodeValue));
                    $notes[] = $note;
                    $doctypeNotes[$id][] = ['id' => $id.'_note_'.++$key, 'article_id' => $id, 'doctype' => self::NOTE_DOC_TYPE, 'note' => $note];
                }
            }
        }

        return $doctypeNotes;
    }

    private function getEntities(DOMXPath $xpath): array
    {
        $entities = [];
        $documentGndsNodes = $xpath->query('//tei:text[@xml:lang="ger"]//tei:name|//tei:rs');
        foreach ($documentGndsNodes as $documentGndsNode) {
            $entityName = trim(preg_replace('/\s+/', ' ', $documentGndsNode->textContent));

            $refValue = $documentGndsNode->getAttribute('ref');
            $typeValue = $documentGndsNode->getAttribute('type');

            if ($refValue && str_contains($refValue, 'gnd:') && $typeValue) {
                $refValue = str_replace('gnd:', '', $refValue);

                $refValueArr = explode(' ', $refValue);

                foreach ($refValueArr as $gnd) {
                    if ((isset($entityName) && !empty($entityName)) && !empty($typeValue) && (isset($gnd) && !empty($gnd))) {
                        $entities[] = [
                            'doctype' => self::ENTITY_DOC_TYPE,
                            'name' => trim($entityName),
                            'entity_type' => $typeValue,
                            'gnd' => $gnd,
                        ];
                    }
                }
            }
        }

        return $entities;
    }

    private function getFulltext(DOMXPath $xpath): string
    {
        $fulltext = '';
        $fulltextNodeList = $xpath->query('//tei:body//tei:div/descendant::text()');
        foreach ($fulltextNodeList as $fulltextNode) {
            $fulltext .= $fulltextNode->data;
        }

        if (!empty($fulltext)) {
            $fulltext = trim(preg_replace('/\s+/', ' ', $fulltext));
        }

        return $fulltext;
    }

    private function getGraphics(array $imageIds, array $imageUrls): array
    {
        $graphics = [];
        if (!empty($imageIds) && !empty($imageUrls)) {
            foreach ($imageIds as $key => $imageId) {
                if (!empty($imageId) && !empty($imageUrls[$key])) {
                    $graphics[$imageId] = $imageUrls[$key];
                }
            }
        }

        return $graphics;
    }

    private function getId(DOMXPath $xpath): string
    {
        $idNode = $xpath->query('//tei:text/@xml:id');
        $id = $idNode->item(0)->nodeValue;

        return $id;
    }

    private function getImageLicense(DOMXPath $xpath): ?string
    {
        $imageLicense = '';

        foreach ($this->imageLicenses as $key => $licenseElement) {
            if (str_contains($this->metadataTransformer->getShelfmark($xpath), $key)) {
                $imageLicense = $licenseElement[0];
            }
        }

        return $imageLicense;
    }

    private function getImageLicenseLink(DOMXPath $xpath): ?string
    {
        $imageLicenseUrl = '';

        foreach ($this->imageLicenses as $key => $licenseElement) {
            if (str_contains($this->metadataTransformer->getShelfmark($xpath), $key)) {
                $imageLicenseUrl = $licenseElement[1];
            }
        }

        return $imageLicenseUrl;
    }

    private function getUuid(): string
    {
        return uuid_create(UUID_TYPE_RANDOM);
    }

    private function indexDocument(DOMDocument $doc, string $file, array $entitiesMap): void
    {
        $xpath = new DOMXPath($doc);
        $xpath->registerNamespace('tei', 'http://www.tei-c.org/ns/1.0');
        $id = $this->getId($xpath);
        $fulltext = $this->getFulltext($xpath);
        $abstracts = $this->getAbstracts($xpath);
        $docType = self::ARTICLE_DOC_TYPE;
        $shortTitle = $this->metadataTransformer->getShortTitle($xpath);
        $title = $this->metadataTransformer->getTitle($xpath);
        $originPlaceGNDNode = $xpath->query('//tei:name[@type="place" and @subtype="orn"]/@ref');

        $pageIds = [];

        if ($originPlaceGNDNode->item(0)) {
            $originPlaceGND = $originPlaceGNDNode->item(0)->nodeValue;
        }

        $originPlace = $this->metadataTransformer->getOriginPlace($xpath);
        $leopoldinaEdition = $this->metadataTransformer->getLeopoldinaEdition($xpath);
        $author = $this->metadataTransformer->getAuthor($xpath);
        $recipient = $this->metadataTransformer->getRecipient($xpath);
        $destinationPlace = $this->metadataTransformer->getDestinationPlace($xpath);
        $originDate = $this->metadataTransformer->getOriginDate($xpath);
        $license = $this->metadataTransformer->getLicense($xpath);
        $language = $this->metadataTransformer->getLanguage($xpath);
        $references = $this->metadataTransformer->getReferences($xpath);
        $responses = $this->metadataTransformer->getResponses($xpath);
        $relatedItems = $this->metadataTransformer->getRelatedItems($xpath);
        $repository = $this->metadataTransformer->getRepository($xpath);
        $institution = $this->metadataTransformer->getInstitution($xpath);
        $settlement = $this->metadataTransformer->getSettlement($xpath);
        $country = $this->metadataTransformer->getCountry($xpath);
        $allEntities = [];
        $allLiterature = [];
        $allNotes = [];

        $institutionDetail = '';
        if (!empty($repository)) {
            $institutionDetail .= $repository;
        }
        if (!empty($institution)) {
            $institutionDetail .= ', '.$institution;
        }
        if (!empty($settlement)) {
            $institutionDetail .= ', '.$settlement;
        }
        if (!empty($country)) {
            $institutionDetail .= ' ('.$country.')';
        }
        if (!empty($institutionDetail)) {
            $institution = trim(preg_replace('/\s+/', ' ', $institutionDetail));
        }

        $sourceDescription = $this->metadataTransformer->getSourceDescription($xpath);
        $publicationDate = $this->metadataTransformer->getPublicationDate($xpath);
        $numberOfPages = $this->metadataTransformer->getNumberOfPages($xpath);
        $gndKeywords = $this->metadataTransformer->getGndKeywords($xpath);
        $freeKeywords = $this->metadataTransformer->getFreeKeywords($xpath);
        $shelfmark = $this->metadataTransformer->getShelfmark($xpath);
        $scriptSource = $this->metadataTransformer->getScriptSource($xpath);
        $writers = $this->metadataTransformer->getWriters($xpath);
        $imageIds = $this->metadataTransformer->getImageIds($xpath);
        $imageUrls = $this->metadataTransformer->getImageUrls($xpath);
        $printSource = $this->metadataTransformer->getPrintSource($xpath);

        $graphics = $this->getGraphics($imageIds, $imageUrls);
        $imageLicense = $this->getImageLicense($xpath);
        $imageLicenseUrl = $this->getImageLicenseLink($xpath);

        $solrDocument = $this->getTextVersions($file, $graphics, $entitiesMap);
        $transcription = $solrDocument->getTranscriptedText();
        $pagesTranscription = $solrDocument->getPageLevelTranscriptedText();
        $editedText = $solrDocument->getEditedText();
        $pagesEdited = $solrDocument->getPageLevelEditedText();
        $pagesGndsUuids = $solrDocument->getPagesGndsUuids();
        $pagesNotes = $solrDocument->getPagesNotes();
        $pagesDates = $solrDocument->getPagesDates();
        $pagesWorks = $solrDocument->getPagesWorks();
        $pagesEntities = $solrDocument->getPagesEntities();
        $pagesEntitiesTypes = $solrDocument->getPagesEntitiesTypes();
        $pagesAllAnnotationIds = $solrDocument->getAllAnnotationIds();
        $pagesLiterature = $solrDocument->getPagesLiterature();
        $pagesFoliants = $solrDocument->getPagesFoliants();
        $pagesGraphics = $this->getPagesGraphics($xpath, $graphics);

        $update = $this->client->createUpdate();
        $doc = $update->createDocument();

        if (!empty($id)) {
            $doc->id = $id;
            $doc->doctype = $docType;

            if (isset($shortTitle) && !empty($shortTitle)) {
                $doc->short_title = $shortTitle;
            }

            if (isset($title) && !empty($title)) {
                $doc->title = $title;
            }

            if (isset($originPlace) && !empty($originPlace)) {
                $doc->origin_place = $originPlace;
            }

            if (isset($leopoldinaEdition) && !empty($leopoldinaEdition)) {
                $doc->leopoldina_edition = $leopoldinaEdition;
            }

            if (isset($author) && !empty($author)) {
                $doc->author = $author;
            }

            if (isset($recipient) && !empty($recipient)) {
                $doc->recipient = $recipient;
            }

            if (isset($originDate) && !empty($originDate)) {
                $doc->origin_date = $originDate;
            }

            if (isset($destinationPlace) && !empty($destinationPlace)) {
                $doc->destination_place = $destinationPlace;
            }

            if (isset($license) && !empty($license)) {
                $doc->license = $license;
            }

            if (isset($language) && !empty($language)) {
                $doc->language = $language;
            }

            if (isset($references) && !empty($references)) {
                $doc->references = $references;
            }

            if (isset($responses) && !empty($responses)) {
                $doc->responses = $responses;
            }

            if (isset($relatedItems) && !empty($relatedItems)) {
                $doc->related_items = $relatedItems;
            }

            if (isset($institution) && !empty($institution)) {
                $doc->institution = $institution;
            }

            if (isset($sourceDescription) && !empty($sourceDescription)) {
                $doc->source_description = $sourceDescription;
            }

            if (isset($publicationDate) && !empty($publicationDate)) {
                $doc->article_pub_date = $publicationDate;
            }

            if (isset($fulltext) && !empty($fulltext)) {
                $doc->fulltext = $fulltext;
            }

            if (isset($numberOfPages) && !empty($numberOfPages)) {
                $doc->number_of_pages = $numberOfPages;
            }

            if (isset($gndKeywords) && !empty($gndKeywords)) {
                $doc->gnd_keyword = $gndKeywords;
            }

            if (isset($freeKeywords) && !empty($freeKeywords)) {
                $doc->free_keyword = $freeKeywords;
            }

            if (isset($shelfmark) && !empty($shelfmark)) {
                $doc->shelfmark = $shelfmark;
            }

            if (isset($scriptSource) && !empty($scriptSource)) {
                $doc->script_source = $scriptSource;
            }

            if (isset($writers) && !empty($writers)) {
                $doc->writer = $writers;
            }

            if (isset($imageIds) && !empty($imageIds)) {
                $doc->image_ids = $imageIds;
            }

            if (isset($imageUrls) && !empty($imageUrls)) {
                $doc->image_urls = $imageUrls;
            }

            if (isset($documentEntities) && !empty($documentEntities)) {
                $doc->entities = $documentEntities;
            }

            if (isset($notes) && !empty($notes)) {
                $doc->notes = $notes;
            }

            if (isset($transcription) && !empty($transcription)) {
                $doc->transcripted_text = $transcription;
            }

            if (isset($editedText) && !empty($editedText)) {
                $doc->edited_text = $editedText;
            }

            if (isset($printSource) && !empty($printSource)) {
                $doc->print_source = $printSource;
                $allLiterature[] = str_replace(' ', '_', explode(',', $printSource)[0]);
            }

            if (!empty($numberOfPages) && $numberOfPages) {
                for ($i = 0; $i < $numberOfPages; ++$i) {
                    $pageNumber = $i + 1;
                    $update1 = $this->client->createUpdate();
                    $childDoc = $update1->createDocument();
                    $childDoc->id = $id.'_page'.$pageNumber;
                    $childDoc->article_id = $id;
                    $childDoc->article_title = $title;
                    $childDoc->doctype = self::PAGE_DOC_TYPE;
                    $childDoc->page_number = $pageNumber;
                    $childDoc->language = $language;

                    if (!empty($imageLicense)) {
                        $childDoc->image_license = $imageLicense;
                    }

                    if (!empty($imageLicenseUrl)) {
                        $childDoc->image_license_url = $imageLicenseUrl;
                    }

                    if (isset($abstracts) && !empty($abstracts)) {
                        $abstractUuid = $this->getUuid();
                        $childDoc->page_notes_abstracts = $abstracts;
                        $childDoc->page_notes_abstracts_ids = [$abstractUuid];
                        if (isset($pagesAllAnnotationIds[$i])) {
                            $pagesAllAnnotationIds[$i] = [$abstractUuid, ...$pagesAllAnnotationIds[$i]];
                        }
                    }

                    if (isset($pagesGraphics[$i]) && !empty($pagesGraphics[$i])) {
                        $childDoc->image_url = $pagesGraphics[$i];
                    }

                    if (isset($pagesTranscription[$i]) && !empty($pagesTranscription[$i])) {
                        $childDoc->transcripted_text = $pagesTranscription[$i];
                    }

                    if (isset($pagesEdited[$i]) && !empty($pagesEdited[$i])) {
                        $childDoc->edited_text = $pagesEdited[$i];
                    }

                    if (isset($pagesGndsUuids[$i]) && !empty($pagesGndsUuids[$i])) {
                        $entities = $pagesGndsUuids[$i];
                        $allEntities = [...$allEntities, ...$entities];
                    }

                    if (isset($pagesEntities[$i]) && !empty($pagesEntities[$i])) {
                        $childDoc->page_entities = array_values($pagesEntities[$i]);
                        $childDoc->page_entities_ids = array_keys($pagesEntities[$i]);
                        $childDoc->page_entities_types = array_values($pagesEntitiesTypes[$i]);
                    }

                    if (isset($pagesNotes[$i]) && !empty($pagesNotes[$i])) {
                        $notes = array_values($pagesNotes[$i]);
                        $allNotes = [...$allNotes, ...$notes];
                        $childDoc->page_notes = $notes;
                        $childDoc->page_notes_ids = array_keys($pagesNotes[$i]);
                    }

                    if (isset($pagesDates[$i]) && !empty($pagesDates[$i])) {
                        $childDoc->page_dates = array_values($pagesDates[$i]);
                        $childDoc->page_dates_ids = array_keys($pagesDates[$i]);
                    }

                    if (isset($pagesWorks[$i]) && !empty($pagesWorks[$i])) {
                        $childDoc->page_works = array_values($pagesWorks[$i]);
                        $childDoc->page_works_ids = array_keys($pagesWorks[$i]);
                    }

                    if (isset($pagesFoliants[$i]) && !empty($pagesFoliants[$i])) {
                        $childDoc->page_foliant = $pagesFoliants[$i];
                    }

                    if (isset($pagesAllAnnotationIds[$i]) && !empty($pagesAllAnnotationIds[$i])) {
                        $childDoc->page_all_annotation_ids = $pagesAllAnnotationIds[$i];
                    }

                    if (isset($pagesLiterature[$i]) && !empty($pagesLiterature[$i])) {
                        $literature = array_values($pagesLiterature[$i]);
                        $childDoc->literature = $literature;
                        $allLiterature = [...$allLiterature, ...$literature];
                    }

                    $pageIds[] = $childDoc->id;
                    $update->addDocument($childDoc);
                }
            }

            $doc->page_ids = $pageIds;
            $doc->all_entities = $allEntities;
            $doc->all_literature = array_unique($allLiterature);
            $doc->all_notes = $allNotes;

            $update->addDocument($doc);
            $update->addCommit();
            $this->client->execute($update);
        }
    }

    private function getPagesGraphics($xpath, $graphicsMap): array
    {
        $pagesGraphics = [];
        $pbNodes = $xpath->query('//tei:body//tei:pb');
        foreach ($pbNodes as $pbNode) {
            /** @var DOMElement $pbNode */
            $facsId = $pbNode->getAttribute('facs');

            $path = null;
            if (isset($facsId) && !empty($facsId) && $facsId !== '#') {
                $facsId = str_replace('#', '', $facsId);
                $path = array_key_exists($facsId, $graphicsMap) ? $graphicsMap[$facsId] : 'path not found at indexing';
            }
            $pagesGraphics[] = $path;
        }

        return $pagesGraphics;
    }

    private function indexEntities(array $entities): array
    {
        $docs = [];
        if (isset($entities) && is_iterable($entities)) {
            foreach ($entities as $entity) {
                if (!empty($entity['gnd'])) {
                    $localFilePath = './data/gnd-files/'.$entity['gnd'].'.json';
                    if (!file_exists($localFilePath)) {
                        $remoteFilePath = 'https://lobid.org/gnd/'.$entity['gnd'].'.json';
                        try {
                            $fileContent = @file_get_contents($remoteFilePath, true);

                            if (false === $fileContent) {
                                throw new Exception($localFilePath);
                            }
                        } catch (Exception $e) {
                            echo $e->getMessage();
                        }

                        // TODO: better exception handling to not go here when remote file path failed
                        $gndArr = [];
                        if ($fileContent) {
                            $filesystem = new Filesystem();
                            $filesystem->dumpFile($localFilePath, $fileContent);

                            $gndArr = json_decode($fileContent);
                        }
                    } else {
                        $gndArr = json_decode(file_get_contents($localFilePath));
                    }

                    if (isset($gndArr->preferredName) && !empty($gndArr->preferredName)) {
                        $preferredName = $gndArr->preferredName;
                    }

                    if (isset($gndArr->variantName) && !empty($gndArr->variantName)) {
                        $variantNames = $gndArr->variantName;
                    }

                    $update = $this->client->createUpdate();
                    $doc = $update->createDocument();
                    $doc->id = $entity['gnd'];
                    $doc->entity_name = $entity['name'];
                    $doc->doctype = $entity['doctype'];
                    $doc->entitytype = $entity['entity_type'];

                    if (isset($preferredName) && !empty($preferredName)) {
                        $doc->preferred_name = $preferredName;
                    }

                    if (isset($variantNames) && !empty($variantNames)) {
                        $doc->variant_names = $variantNames;
                    }

                    $docs[] = $doc;
                    $update->addDocument($doc);
                    $update->addCommit();
                    $this->client->execute($update);
                }
            }
        }

        return $docs;
    }

    private function indexNotes(array $doctypeNotes): void
    {
        if (isset($doctypeNotes) && is_iterable($doctypeNotes)) {
            foreach ($doctypeNotes as $doctypeNoteArr) {
                foreach ($doctypeNoteArr as $doctypeNote) {
                    if (!empty($doctypeNote['id'])) {
                        $update = $this->client->createUpdate();
                        $doc = $update->createDocument();
                        $doc->id = $doctypeNote['id'];
                        $doc->article_id = $doctypeNote['article_id'];
                        $doc->doctype = $doctypeNote['doctype'];
                        $doc->note = $doctypeNote['note'];
                        $update->addDocument($doc);
                        $update->addCommit();
                        $this->client->execute($update);
                    }
                }
            }
        }
    }

    private function removeHyphen(string $text): string
    {
        $pattern = '/(\w+)-\s(\w)/i';
        $text = preg_replace_callback(
            $pattern,
            fn ($match) => $match[1].$match[2],
            $text
        );

        return $text;
    }
}
