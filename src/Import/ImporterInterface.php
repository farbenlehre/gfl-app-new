<?php

namespace App\Import;

interface ImporterInterface
{
    public function importImagesToS3Storage(): string;

    public function importTeisForIndexing(string $server): void;

    public function importTeiToS3Storage(): void;
}
