<?php

declare(strict_types=1);

namespace App\Import;

use App\Service\FileService;
use Exception;
use Imagine\Exception\RuntimeException;
use Imagine\Image\ImageInterface;
use Imagine\Imagick\Imagine;
use Sabre\DAV\Client;
use Sabre\DAV\Xml\Property\ResourceType;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class Importer implements ImporterInterface
{
    private const s3ImageExtension = 'jpg';
    private FileService $fileService;
    private ?string $gitlabLitRepoUrl;
    private ?string $gitlabProcessedLitRepoUrl;
    private ?string $gitlabProcessedTeiRepoUrl;
    private ?string $gitlabRepoToken;
    private ?string $gitlabRepoTreeUrl;
    private ?string $invalidTeiListFile;
    private ?string $litDir;
    private ?string $owncloudBaseUrl = null;
    private ?string $owncloudImageArchive = null;
    private ?string $owncloudImagesDir = null;
    private ?string $owncloudPassword;
    private ?string $owncloudRootDir = null;
    private ?string $owncloudUsername;
    private ?string $sampleTeiDocumentUrl;
    private ?string $teiDir = null;
    private ?string $teiSampleDir = null;

    public function __construct(FileService $fileService, ParameterBagInterface $params)
    {
        $this->fileService = $fileService;
        $this->setConfigs(
            $params->get('tei_dir'),
            $params->get('tei_sample_dir'),
            $params->get('lit_dir'),
            $params->get('owncloud_images_dir'),
            $params->get('owncloud_base_url'),
            $params->get('owncloud_root_dir'),
            $params->get('owncloud_image_archive'),
            $params->get('owncloud_username'),
            $params->get('owncloud_password'),
            $params->get('gitlab_repo_token'),
            $params->get('gitlab_repo_tree_url'),
            $params->get('gitlab_processed_tei_repo_url'),
            $params->get('invalid_tei_list_file'),
            $params->get('sample_tei_document_url'),
            $params->get('gitlab_lit_repo_url'),
            $params->get('gitlab_processed_lit_repo_url'),
        );
    }

    public function importImagesToS3Storage(): string
    {
        $settings = [
            'baseUri' => $this->owncloudBaseUrl,
            'userName' => $this->owncloudUsername,
            'password' => $this->owncloudPassword,
        ];

        $this->client = new Client($settings);

        try {
            $this->importImagesFromOwncloudToS3Storage();
        } catch (Exception $e) {
            $message = $e->getMessage();
            if (strlen($message) > 2000) {
                $message = substr($message, 0, 400).' [...] '.substr($message, -1500);
            }

            return $message;
        }

        return '';
    }

    public function importLiterature(): void
    {
        $filesystem = new Filesystem();

        if (!$filesystem->exists($this->litDir)) {
            $filesystem->mkdir($this->litDir);
        }

        try {
            $files = file_get_contents($this->gitlabLitRepoUrl.'&page=1&access_token='.$this->gitlabRepoToken);

            if (is_string($files)) {
                $files = json_decode($files, true);

                foreach ($files as $file) {
                    $teiFileUrl = $this->gitlabProcessedLitRepoUrl.$file['name'].'?access_token='.$this->gitlabRepoToken.'&ref=master';
                    $fileData = file_get_contents($teiFileUrl);

                    if (is_string($fileData)) {
                        $fileData = json_decode($fileData, true);

                        try {
                            $filesystem->dumpFile(
                                $this->litDir.$file['name'],
                                base64_decode($fileData['content'])
                            );
                        } catch (FileException $exception) {
                            echo $file['name'].' could not be imported.';
                        }
                    } else {
                        // TODO retry to download the file again
                        echo $file['name'].' could not be imported.';
                    }
                }
            }
        } catch (FileException $exception) {
            echo 'Literature files list could not be imported from gitlab';
        }
    }

    public function importTeisForIndexing(string $server): void
    {
        if ('dev' === $server) {
            $this->importSampleTeiDocument();
        }

        $this->importLiterature();
        $filesystem = new Filesystem();
        if (!$filesystem->exists($this->teiDir)) {
            $filesystem->mkdir($this->teiDir);
        }
        $invalidTeiList = $this->getInvalidTeiList();

        # TODO: remove this since the volltext data repo is now added as git submodule
        #       at /data/volltexte. The TEI files to load are present in 
        #       /data/volltexte/TEI_doc_bearb/ dir.
        for ($i = 1; $i <= 100; ++$i) {
            try {
                $files = file_get_contents($this->gitlabRepoTreeUrl.'&access_token='.$this->gitlabRepoToken.'&page='.$i);

                if (is_string($files)) {
                    $files = json_decode($files, true);

                    foreach ($files as $file) {
                        if (!in_array(trim($file['name']), $invalidTeiList)) {
                            $teiFileUrl = $this->gitlabProcessedTeiRepoUrl.$file['name'].'?access_token='.$this->gitlabRepoToken.'&ref=master';
                            $fileData = file_get_contents($teiFileUrl);
                            if (is_string($fileData)) {
                                $fileData = json_decode($fileData, true);

                                try {
                                    $filesystem->dumpFile(
                                        $this->teiDir.$file['name'],
                                        base64_decode($fileData['content'])
                                    );
                                } catch (FileException $exception) {
                                    echo $file['name'].' could not be imported.';
                                }
                            } else {
                                // TODO retry to download the file again
                                echo $file['name'].' could not be imported.';
                            }
                        }
                    }
                }
            } catch (FileException $exception) {
                echo 'Files list could not be imported from gitlab';
            }
        }
    }

    public function importTeiToS3Storage(): void
    {
        $mainFileSystem = $this->fileService->getMainFilesystem();
        $mainFileSystem->deleteDir('tei');
        $teiFilesystem = $this->fileService->getTeiFilesystem();
        $sampleTeiDocument = $this->getSampleTEIDocument();

        if (!empty($sampleTeiDocument)) {
            $teiFilesystem->write('sample.xml', $sampleTeiDocument);
        }

        $filesystem = new Filesystem();

        if (!$filesystem->exists($this->teiDir)) {
            $filesystem->mkdir($this->teiDir);
        }

        $invalidTeiList = $this->getInvalidTeiList();

        for ($i = 1; $i <= 100; ++$i) {
            try {
                $files = file_get_contents($this->gitlabRepoTreeUrl.'&access_token='.$this->gitlabRepoToken.'&page='.$i);

                if (is_string($files)) {
                    $files = json_decode($files, true);

                    foreach ($files as $file) {
                        if (!in_array(trim($file['name']), $invalidTeiList)) {
                            $teiFileUrl = $this->gitlabProcessedTeiRepoUrl.$file['name'].'?access_token='.$this->gitlabRepoToken.'&ref=master';
                            $this->downloadTeiFile($teiFilesystem, $file, $teiFileUrl);
                        }
                    }
                }
            } catch (FileException $exception) {
                echo 'Files list could not be imported from gitlab';
            }
        }
    }

    public function setConfigs(string $teiDir, string $teiSampleDir, string $litDir, string $owncloudImagesDir, string $owncloudBaseUrl, string $owncloudRootDir, string $owncloudImageArchive, string $owncloudUsername, string $owncloudPassword, string $gitlabRepoToken, string $gitlabRepoTreeUrl, string $gitlabProcessedTeiRepoUrl, string $invalidTeiListFile, string $sampleTeiDocumentUrl, string $gitlabLitRepoUrl, string $gitlabProcessedLitRepoUrl): void
    {
        $this->teiDir = $teiDir;
        $this->teiSampleDir = $teiSampleDir;
        $this->litDir = $litDir;
        $this->owncloudImagesDir = $owncloudImagesDir;
        $this->owncloudBaseUrl = $owncloudBaseUrl;
        $this->owncloudRootDir = $owncloudRootDir;
        $this->owncloudImageArchive = $owncloudImageArchive;
        $this->owncloudUsername = $owncloudUsername;
        $this->owncloudPassword = $owncloudPassword;
        $this->gitlabRepoToken = $gitlabRepoToken;
        $this->gitlabRepoTreeUrl = $gitlabRepoTreeUrl;
        $this->gitlabProcessedTeiRepoUrl = $gitlabProcessedTeiRepoUrl;
        $this->invalidTeiListFile = $invalidTeiListFile;
        $this->sampleTeiDocumentUrl = $sampleTeiDocumentUrl;
        $this->gitlabLitRepoUrl = $gitlabLitRepoUrl;
        $this->gitlabProcessedLitRepoUrl = $gitlabProcessedLitRepoUrl;
    }

    private function downloadTeiFile($teiFilesystem, array $file, string $teiFileUrl): bool
    {
        try {
            $fileDownload = false;
            $fileData = file_get_contents($teiFileUrl);
            if (is_string($fileData)) {
                $fileData = json_decode($fileData, true);

                try {
                    $teiFilesystem->write($file['name'], base64_decode($fileData['content']));
                } catch (FileException $exception) {
                    echo $file['name'].' could not be imported into S3.';
                    $this->downloadTeiFile($teiFilesystem, $file, $teiFileUrl);
                }

                $fileDownload = true;
            }
        } catch (FileException $exception) {
            echo $file['name'].' could not be imported. Try again.';
            $this->downloadTeiFile($teiFilesystem, $file, $teiFileUrl);
        }

        return $fileDownload;
    }

    private function getImagePaths(): array
    {
        $path = $this->owncloudRootDir.'/'.$this->owncloudImageArchive;

        $content = $this->client->propFind($path, [
            '{DAV:}getlastmodified',
            '{DAV:}resourcetype',
        ], 10);

        $keys = array_keys($content);
        array_shift($keys);
        $images = [];

        foreach ($keys as $key) {
            /* @var ResourceType $resType; */
            $type = $content[$key]['{DAV:}resourcetype'];

            if (!(null !== $type && $type->is('{DAV:}collection'))) {
                $segments = explode('/', $key);
                $segments = array_values(array_filter($segments));
                $length = count($segments) - 3;
                $offset = 3;
                $segments = array_slice($segments, $offset, $length);
                $imagePath = implode('/', $segments);
                $images[] = ['name' => $imagePath];
            }
        }

        return $images;
    }

    private function getInvalidTeiList(): array
    {
        $invalidTeiList = [];
        $file_headers = @get_headers($this->invalidTeiListFile);
        if ('HTTP/1.1 200 OK' === $file_headers[0]) {
            $fileContent = file_get_contents($this->invalidTeiListFile);

            if ($fileContent) {
                $invalidTeiList = json_decode(file_get_contents($this->invalidTeiListFile), true);
            }
        }

        return $invalidTeiList;
    }

    private function getSampleTeiDocument(): ?string
    {
        $sampleTeiDocument = '';
        $sampleTeiDocumentUrl = $this->sampleTeiDocumentUrl.'&access_token='.$this->gitlabRepoToken;
        $file_headers = @get_headers($sampleTeiDocumentUrl);
        if ('HTTP/1.1 200 OK' === $file_headers[0]) {
            $sampleDocumentData = json_decode(file_get_contents($sampleTeiDocumentUrl), true);
            $sampleTeiDocument = base64_decode($sampleDocumentData['content']);
        }

        return $sampleTeiDocument;
    }

    private function importImagesFromOwncloudToS3Storage()
    {
        $imagesPaths = $this->getImagePaths();
        $downloadedImages = [];
        $importedImages = [];

        if (is_array($imagesPaths) && !empty($imagesPaths)) {
            foreach ($imagesPaths as $imagesPath) {
                if ('' !== $imagesPath) {
                    $convertedImagePath = urldecode(
                        str_replace(
                            ['Graph_Archiv', 'TIF', 'tif'],
                            ['', 'jpg', 'jpg'],
                            $imagesPath['name']
                        )
                    );

                    $imageFilesystem = $this->fileService->getImageFilesystem();

                    if (!$imageFilesystem->has($convertedImagePath)) {
                        $imageName = array_reverse(explode('/', $imagesPath['name']))[0];
                        $imageName = urldecode($imageName);
                        $extension = pathinfo($imageName, PATHINFO_EXTENSION);

                        if ('tif' === $extension) {
                            $response = $this->client->request('GET', $this->owncloudRootDir.'/'.$imagesPath['name']);

                            if (!$response['body']) {
                                return;
                            }

                            $filesystem = new Filesystem();

                            if (!$filesystem->exists($this->owncloudImagesDir)) {
                                $filesystem->mkdir($this->owncloudImagesDir);
                            }

                            $filesystem->dumpFile(
                                $this->owncloudImagesDir.$imageName,
                                $response['body']
                            );

                            $convertedImagePath = trim($convertedImagePath, '/');
                            $this->saveJpegToS3($convertedImagePath, $imageName);
                            $importedImages[] = $convertedImagePath;
                        }
                    }
                }
            }
        }

        if (is_array($importedImages) && !empty($importedImages)) {
            echo 'The following images were imported into S3 storage:'.PHP_EOL;
            foreach ($importedImages as $importedImage) {
                echo $importedImage.PHP_EOL;
            }
        }
    }

    private function importSampleTeiDocument(): void
    {
        $sampleTeiDocument = $this->getSampleTeiDocument();
        $filesystem = new Filesystem();
        if (!empty($sampleTeiDocument)) {
            $filesystem->dumpFile($this->teiSampleDir.'sample.xml', $sampleTeiDocument);
        }
    }

    private function saveJpegToS3(string $convertedImagePath, string $downloadedImage)
    {
        $imageFilesystem = $this->fileService->getImageFilesystem();
        $filesystem = new Filesystem();

        if (file_exists($this->owncloudImagesDir.$downloadedImage)) {
            try {
                $options = [
                    'resolution-units' => ImageInterface::RESOLUTION_PIXELSPERINCH,
                    'resolution-x' => 150,
                    'resolution-y' => 150,
                    'jpeg_quality' => 50,
                ];
                $jpgImageName = explode('.', $downloadedImage)[0];
                $imagine = new Imagine();

                $imagine
                    ->open($this->owncloudImagesDir.$downloadedImage)
                    ->save($this->owncloudImagesDir.$jpgImageName.'.'.self::s3ImageExtension, $options);

                $file = file_get_contents($this->owncloudImagesDir.$jpgImageName.'.'.self::s3ImageExtension);
                $imageFilesystem->put($convertedImagePath, $file);
                $filesystem->remove($this->owncloudImagesDir.$downloadedImage);
                $filesystem->remove($this->owncloudImagesDir.$jpgImageName.'.'.self::s3ImageExtension);
            } catch (RuntimeException $e) {
                echo 'Msg:'.$e->getMessage();
            }
        }
    }
}
