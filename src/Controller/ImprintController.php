<?php

namespace App\Controller;

use Parsedown;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ImprintController extends AbstractController
{
    /**
     * @Route("impressum", name="_imprint"),
     */
    public function imprint(): Response
    {
        $parsedown = new Parsedown();

        $file = sprintf('%s/../../assets/content/%s.md', __DIR__, 'imprint');

        if (file_exists($file)) {
            $content = file_get_contents($file);
        } else {
            throw new NotFoundHttpException(sprintf('Page %s not found.', 'imprint'));
        }

        return $this->render('pages/imprint/imprint.html.twig',
            [
                'content' => $parsedown->text($content),
            ]
        );
    }
}
