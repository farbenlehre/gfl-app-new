<?php

namespace App\Controller;

use App\Service\SearchServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Exception\InvalidParameterException;

class RegisterController extends AbstractController
{
    private SearchServiceInterface $client;
    private ParameterBagInterface $params;

    public function __construct(SearchServiceInterface $client, ParameterBagInterface $params)
    {
        $this->client = $client;
        $this->params = $params;
    }

    /**
     * @Route("/register", name="_register")
     */
    public function register(Request $request): Response
    {
        $allEntityTypesSorted = ['person', 'place', 'org', 'object'];

        $filterParam = $request->get('filter');

        $entityTypesFilters = $filterParam ?? $allEntityTypesSorted;

        if (!is_array($entityTypesFilters)) {
            throw new InvalidParameterException('Incorrect filter argument.', 1_549_013_378);
        }

        $entities = $this->client->getEntities();

        $entityTypesInResult = [];
        $filteredEntities = [];
        $gndBaseUrl = $this->params->get('gnd_base_url');
        foreach ($entities as $entity) {
            // Identify entity types that exist in the result
            if (!in_array($entity['entitytype'], $entityTypesInResult)) {
                $entityTypesInResult[] = $entity['entitytype'];
            }

            // Collect entities that match the request filter param
            if (in_array($entity['entitytype'], $entityTypesFilters)) {
                $entity['name'] = $entity['preferred_name'] ?? (isset($entity['variant_names']) ? $entity['variant_names'][0] : 'Name not avaiable');
                $entity['url'] = $this->getGNDUrl($entity['id']);
                $filteredEntities[] = $entity;
            }
        }

        usort($filteredEntities, function ($a, $b) {
            return strcmp($a['name'], $b['name']);
        });

        // Create entity types array for the filter form
        $filtersFormData = [];
        foreach ($allEntityTypesSorted as $entityTypeSorted) {
            if (in_array($entityTypeSorted, $entityTypesInResult)) {
                $filtersFormData[] = [
                    'type' => $entityTypeSorted,
                    'checked' => in_array($entityTypeSorted, $entityTypesFilters),
                    'value' => $entityTypeSorted,
                ];
            }
        }

        return $this->render('pages/lists/register.html.twig',
            [
                'entities' => $filteredEntities,
                'filters' => $filtersFormData,
            ]
        );
    }

    /**
     * @Route("/register/{id}", name="_register_item")
     */
    public function registerItem(string $id, Request $request)
    {
        $entities = $this->client->getEntitiesbyId($id);
        foreach ($entities as $index => $entity) {
            $entities[$index]['name'] = $entity['preferred_name'] ?? (isset($entity['variant_names']) ? $entity['variant_names'][0] : 'Name not avaiable');
            $entities[$index]['url'] = $this->getGNDUrl($entity['id']);
        }

        return $this->render('pages/lists/register_item.html.twig',
            [
                'occurrences' => $this->client->getDocumentsFromEntities($id),
                'entities' => $entities,
            ]
        );
    }

    private function getGNDUrl(string $gndId): string
    {
        $gndBaseUrl = $this->params->get('gnd_base_url');

        return $gndBaseUrl.$gndId;
    }
}
