<?php

namespace App\Controller;

use Parsedown;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class PrivacyController extends AbstractController
{
    /**
     * @Route("datenschutz", name="_privacy"),
     */
    public function privacy(): Response
    {
        $parsedown = new Parsedown();

        $file = sprintf('%s/../../assets/content/%s.md', __DIR__, 'privacy');

        if (file_exists($file)) {
            $content = file_get_contents($file);
        } else {
            throw new NotFoundHttpException(sprintf('Page %s not found.', 'privacy'));
        }

        return $this->render('pages/privacy/privacy.html.twig',
            [
                'content' => $parsedown->text($content),
            ]
        );
    }
}
