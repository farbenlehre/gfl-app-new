<?php

namespace App\Controller;

use App\Service\SearchServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LiteratureController extends AbstractController
{
    private SearchServiceInterface $client;

    public function __construct(SearchServiceInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @Route("/literatur", name="_literature")
     */
    public function literature(): Response
    {
        return $this->render('pages/lists/literature.html.twig',
            [
                'content' => $this->client->getLiterature(),
            ]
        );
    }

    /**
     * @Route("/literatur/{id}", name="_literature_item")
     */
    public function literatureItem(string $id): Response
    {
        return $this->render('pages/lists/literature_item.html.twig',
            [
                'item' => $this->client->getLiteratureItem($id),
                'occurrences' => $this->client->getDocumentsFromLiterature($id),
            ]
        );
    }
}
