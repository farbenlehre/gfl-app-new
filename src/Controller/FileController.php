<?php

namespace App\Controller;

use App\Model\Downloadable;
use App\Service\FileService;
use App\Service\SearchServiceInterface;
use League\Flysystem\DirectoryAttributes;
use League\Flysystem\FileNotFoundException;
use League\Flysystem\FilesystemException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/download/")
 */
class FileController extends AbstractController
{
    private SearchServiceInterface $client;
    private FileService $fileService;

    private ?array $imageArchives = null;

    private string $mainDomain;
    private TranslatorInterface $translator;

    public function __construct(
        FileService $fileService,
        SearchServiceInterface $client,
        ParameterBagInterface $params,
        TranslatorInterface $translator
    )
    {
        $this->fileService = $fileService;
        $this->client = $client;

        $this->imageArchives = $params->get('image_archives');
        $this->mainDomain = $params->get('main_domain');
        $this->translator = $translator;
    }

    /**
     * @Route("archivalien", name="_imagearchivelist")
     */
    public function imageArchiveList(): Response
    {
        $imageFilesystem = $this->fileService->getImageFilesystem();
        $imageArchives = $imageFilesystem->listContents('.');
        $imageArchivesArr = [];

        foreach ($imageArchives as $imageArchive) {
            /** @var DirectoryAttributes $imageArchive */
            $path = $imageArchive->path();
            if (array_key_exists($path, $this->imageArchives)) {
                $imageArchivesArr[$path] = $this->imageArchives[$path];
            }
        }

        return $this->render('pages/lists/archives.html.twig',
            [
                'archives' => $imageArchivesArr,
            ]
        );
    }

    /**
     * @Route("image/{archive}/{subarchive}/{imagefile}", name="_imagefile")
     */
    public function imageFile(string $archive, string $subarchive, string $imagefile)
    {
        $imageFilesystem = $this->fileService->getImageFilesystem();

        if ($imagefile !== $subarchive) {
            $filepath = sprintf('%s/%s/%s.%s', $archive, $subarchive, $imagefile, 'jpg');
        } else {
            $filepath = sprintf('%s/%s.%s', $archive, $imagefile, 'jpg');
        }

        try {
            $image = $imageFilesystem->read($filepath);
        } catch (FileNotFoundException $e) {
            throw new NotFoundHttpException(sprintf('Image not found.'));
        }

        $response = new Response();
        $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_INLINE, $imagefile . '.jpg');
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Type', 'image/jpg');
        $response->setContent($image);

        return $response;
    }

    /**
     * @Route("imagesubarchivelist/{archive}", name="_imagesubarchivelist")
     */
    public function imageSubArchiveList(string $archive): Response
    {
        $imageFilesystem = $this->fileService->getImageFilesystem();
        $imageSubArchives = $imageFilesystem->listContents($archive);

        return $this->render('pages/lists/subarchives.html.twig',
            [
                'archive' => $archive,
                'archiveName' => $this->imageArchives[$archive],
                'sub_archives' => $imageSubArchives,
            ]
        );
    }

    /**
     * @Route("imagesubsubarchivelist/{archive}/{subArchive}", name="_imagesubsubarchivelist")
     */
    public function imageSubSubArchiveList(string $archive, string $subArchive): Response
    {
        $imageFilesystem = $this->fileService->getImageFilesystem();
        $imageSubSubArchives = $imageFilesystem->listContents($archive . '/' . $subArchive);

        $pageData = [];

        foreach ($imageSubSubArchives as $imageDataSet) {
            $imagePath = 'Graph/' . $imageDataSet['path'];

            if (!empty($this->getPageData($imagePath)['article_id'])) {
                $pageData[$imageDataSet['filename']]['documentId'] = $this->getPageData($imagePath)['article_id'];
            } else {
                $pageData[$imageDataSet['filename']]['documentId'] = '';
            }

            if (!empty($this->getPageData($imagePath)['page_number'])) {
                $pageData[$imageDataSet['filename']]['itemUrl'] = $this->getItemUrl($this->getPageData($imagePath)['id']);
            } else {
                $pageData[$imageDataSet['filename']]['itemUrl'] = '';
            }
        }

        return $this->render('pages/lists/archive.html.twig',
            [
                'archiveName' => $this->imageArchives[$archive],
                'archive' => $archive,
                'subarchive' => $subArchive,
                'ImageArchive' => $imageSubSubArchives,
                'pageData' => $pageData,
            ]
        );
    }

    /**
     * @Route("leopoldina", name="_leopoldina")
     */
    public function leopoldina(): Response
    {
        $url1 = 'https://textgridlab.org/1.0/tgcrud-public/rest/textgrid:3zhh5/data#page=2';
        $url2 = 'https://textgridlab.org/1.0/tgcrud-public/rest/textgrid:3zhh6/data#page=3';

        $files = [
            new Downloadable(
                $this->translator->trans('leopoldina_page.link_file_name_1'),
                $url1,
                $this->translator->trans('leopoldina_page.link_title_1'),
                6291456
            ),
            new Downloadable(
                $this->translator->trans('leopoldina_page.link_file_name_2'),
                $url2,
                $this->translator->trans('leopoldina_page.link_title_2'),
                63963136
            )
        ];

        return $this->render('pages/leopoldina/leopoldina.html.twig', ['files' => $files]);
    }

    /**
     * @Route("lit/{id}", name="_litfile")
     */
    public function lit(string $id): Response
    {
        try {
            $lit = $this->client->getLiteratureItem($id);
        } catch (FileNotFoundException $e) {
            throw new NotFoundHttpException(sprintf('Literature with ID "%s" not found.', $id));
        }

        $response = new Response();
        $response->headers->set('Content-Type', 'text/xml');
        $response->setContent($lit['xml']);

        return $response;
    }

    /**
     * @Route("pdf/{pdfdir}/{pdffile}", name="_pdffile")
     */
    public function pdfFile(string $pdffile, string $pdfdir = 'download'): Response
    {
        $pdfFilesystem = $this->fileService->getPdfFilesystem();
        $filepath = sprintf('%s/%s.%s', $pdfdir, $pdffile, 'pdf');

        try {
            $pdf = $pdfFilesystem->read($filepath);
        } catch (FileNotFoundException $e) {
            throw new NotFoundHttpException('File not found.');
        }

        $response = new Response();
        $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_INLINE, $pdffile . '.pdf');
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Type', 'application/pdf');
        $response->setContent($pdf);

        return $response;
    }

    /**
     * @Route("tei/{filename}", name="_teifile")
     */
    public function tei(string $filename): Response
    {
        $filenameArr = explode('.', $filename);

        if (!isset($filenameArr[1])) {
            $extension = 'xml';
        } else {
            $extension = $filenameArr[1];
        }

        try {
            $teiFilesystem = $this->fileService->getTeiFilesystem();
            $tei = $teiFilesystem->read($filenameArr[0] . '.' . $extension);
        } catch (FileNotFoundException $e) {
            throw new NotFoundHttpException(sprintf('File %s not found.', $filename));
        }

        $response = new Response();
        $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_INLINE, $filename);
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Type', 'text/xml');
        $response->setContent($tei);

        return $response;
    }

    private function getItemUrl($pageId)
    {
        return $this->mainDomain . '/tido/gfl/' . $pageId . '/latest/item.json';
    }

    private function getPageData($imagePath): array
    {
        return $this->client->getPageData($imagePath);
    }

    private function remoteFileSize($url): ?int
    {
        $data = get_headers($url, true);
        if (isset($data['Content-Length']))
            return (int)$data['Content-Length'];

        return 0;
    }
}
