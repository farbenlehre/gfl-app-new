<?php

namespace App\Controller;

use App\Service\SearchServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class InfoController extends AbstractController
{
    /*
     * @var SearchServiceInterface
     */
    private \App\Service\SearchServiceInterface $client;

    public function __construct(SearchServiceInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @Route("/info", name="_info"),
     */
    public function info()
    {
        return $this->render('pages/info/info.html.twig',
            [
                'info' => $this->client->getInfo(),
            ]
        );
    }
}
