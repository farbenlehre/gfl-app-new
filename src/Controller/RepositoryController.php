<?php

namespace App\Controller;

use App\Service\SearchServiceInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Exception\InvalidParameterException;

class RepositoryController extends AbstractController
{
    private SearchServiceInterface $client;

    private string $mainDomain;

    private ?int $numberOfSearchResults = 10;

    private PaginatorInterface $paginator;

    public function __construct(SearchServiceInterface $client, PaginatorInterface $paginator, ParameterBagInterface $params)
    {
        $this->client = $client;
        $this->paginator = $paginator;
        $this->mainDomain = $params->get('main_domain');
    }

    /**
     * @Route("/repositorium/{id}", name="_repository_detail", defaults={"back_url": ""})
     */
    public function detail(string $id, string $back_url, Request $request): Response
    {
        if (strstr($id, '#')) {
            $id = rtrim($id, '#');
        }

        $manifestUrl = $this->generateUrl('subugoe_text_api_manifest', ['manifest' => $id]);

        return $this->render('pages/repository/detail.html.twig',
            [
                'mainDomain' => $this->mainDomain,
                'manifestUrl' => $manifestUrl,
                'backUrl' => $back_url
            ]
        );
    }

    /**
     * @Route("/repositorium", name="_repository")
     */
    public function search(Request $request): Response
    {
      $queryArr = $request->get('q');
      if (empty($queryArr)) {
          $queryArr = [[
            'operator' => 'none',
            'field' => 'title',
            'value' => '*',
            'phrase' => false,
          ]];
      }

      $sort = $request->get('sorting');

      if (isset($sort) && empty($sort)) {
          throw new InvalidParameterException('Sort argument must be provided.', 1_549_013_378);
      }

      // Map URL params to Solr fields
      if ('date' === $sort) {
          $sort = 'origin_date';
      }

      $searchNotes = $request->get('search_notes');

      $data = $this->client->search($queryArr, $sort, $request->get('direction'), $searchNotes);

      $documents = $data['response']['docs'];

      $currentPage = empty($request->get('page')) ? 1 : (int) $request->get('page');
      $paginatedResults = $this->paginator->paginate($documents, $currentPage, $this->numberOfSearchResults);

      $resultsTotal = $paginatedResults->getTotalItemCount();
      $resultsPerPage = $paginatedResults->getItemNumberPerPage();
      $resultsFrom = ($paginatedResults->getCurrentPageNumber() - 1) * $paginatedResults->getItemNumberPerPage() + 1;
      $resultsTo = ($resultsTotal <= $resultsPerPage) ? $resultsTotal : $paginatedResults->getCurrentPageNumber() * $resultsPerPage;

      $fullQueryString = $request->getQueryString();

      return $this->render('pages/repository/repository.html.twig',
            [
                'documents' => $paginatedResults ?? [],
                'queryParams' => $request->get('filter') ?? [],
                'from' => $from ?? null,
                'to' => $to ?? null,
                'searchNotes' => $searchNotes ?? null,
                'query' => $fullQueryString,
                'resultsFrom' => $resultsFrom,
                'resultsTo' => $resultsTo,
                'resultsTotal' => $resultsTotal,
            ]
        );
    }
}
