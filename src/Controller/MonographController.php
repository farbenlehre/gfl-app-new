<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MonographController extends AbstractController
{
    private ?string $contentDir = null;
    private string $downloadBasePath = 'download/pdf/monograph/';

    public function __construct(ParameterBagInterface $params)
    {
        $this->contentDir = $params->get('content_dir');
    }

    /**
     * @Route("leittexte", name="_monograph")
     */
    public function monograph(): Response
    {
        $finder = new Finder();
        $finder->files()->in($this->contentDir)->name('monograph.json');
        $json = null;
        foreach ($finder as $file) {
            $json = json_decode($file->getContents(), true);
        }

        return $this->render(
            'pages/monograph/monograph.html.twig',
            [
                "sections" => $this->recursiveSection($json)
            ]
        );
    }

    private function recursiveSection($data, $level = 1): string
    {
        $template = "";
        $hSize = $level + 1;

        foreach ($data as $key => $value) {
            if (is_string($value)) {
                $url = $this->downloadBasePath . $value;
                $template .= "<a title=\"$value\" href=\"$url\">$key<i class=\"fa fa-file-pdf\"></i></a>";
            } else {
                $template .= "<h$hSize>$key</h$hSize>";
                if (is_array($value) && !empty($value)) {
                    $template .= "<section>";
                    $template .= $this->recursiveSection($value, $level + 1);
                    $template .= "</section>";
                }
            }
        }

        return $template;
    }
}
