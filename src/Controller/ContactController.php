<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    /**
     * @Route("kontakt", name="_contact"),
     */
    public function contact(): Response
    {
        return $this->render('pages/contact/contact.html.twig');
    }
}
