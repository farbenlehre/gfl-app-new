<?php

namespace App\Controller;

use App\Service\FileService;
use League\Flysystem\FileNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ImageController.
 */
class ImageController extends AbstractController
{
    private FileService $fileService;

    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
    }


    #[Route("Graph/{archive}/{document}/{page_id}.{format}", name: "_image")]
    public function image(string $archive, string $document, string $page_id, string $format): Response
    {
        $sourceFilesystem = $this->fileService->getImageFilesystem();
        $filepath = sprintf('%s/%s/%s.%s', $archive, $document, $page_id, $format);

        $filename = sprintf('%s.%s', $page_id, $format);
        try {
            $image = $sourceFilesystem->read($filepath);
        } catch (FileNotFoundException $e) {
            throw new NotFoundHttpException(sprintf('Image %s.%s not found.', $page_id, $format));
        }

        $response = new Response();
        $disposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_INLINE, $filename);
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Type', 'image/jpg');
        $response->setContent($image);

        return $response;
    }
}
