var Encore = require('@symfony/webpack-encore');
var ImageminPlugin = require('imagemin-webpack-plugin').default;

Encore
    .setOutputPath('public/build/')
    .setPublicPath('/build')
    .addEntry('app', './assets/js/app.js')
    .addEntry('tido', './node_modules/@subugoe/tido/dist/tido.js')
    .addStyleEntry('tido_style', '/node_modules/@subugoe/tido/dist/tido.css')
    .addStyleEntry('support', './assets/scss/support.scss')
    .addPlugin(new ImageminPlugin({ test: /\.(jpe?g|png|gif|svg)$/i }))
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())
    .enableSassLoader()
    .disableSingleRuntimeChunk();

module.exports = Encore.getWebpackConfig();
