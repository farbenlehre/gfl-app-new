<?php

declare(strict_types=1);

use Rector\Core\Configuration\Option;
use Rector\Set\ValueObject\SetList;
use Rector\Symfony\Set\SymfonySetList;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function (ContainerConfigurator $containerConfigurator): void {
    $parameters = $containerConfigurator->parameters();

    $parameters->set(Option::IMPORT_SHORT_CLASSES, false);
    $containerConfigurator->import(SetList::PHP_74);
    $containerConfigurator->import(SymfonySetList::SYMFONY_52);
    $containerConfigurator->import(SymfonySetList::SYMFONY_CODE_QUALITY);

    $parameters->set(Option::PATHS, [
        'src/',
        'tests/',
    ]);

    $parameters->set(Option::SKIP, [
        '*/src/Migrations/*',
        '*/vendor/*',
        '*/var/*',
        '*/public/*',
        '*/config/*',
    ]);
};
