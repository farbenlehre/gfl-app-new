# Goethes Farbenlehre Web App

The Application 


## Requirements

You need PHP 7.4 with cURL, DOMDocument, gd and ImageMagick installed.
Also see [http://symfony.com/doc/current/reference/requirements.html](http://symfony.com/doc/current/reference/requirements.html).

For JavaScript dependencies [yarn](https://yarnpkg.com) is needed and [nvm](https://github.com/creationix/nvm) is recommended.

For running the application docker and docker-compose are also required.

Docker installation instruction:
    ubuntu: https://docs.docker.com/engine/install/ubuntu/
    windows: https://docs.docker.com/desktop/windows/install/
    Mac: https://docs.docker.com/desktop/mac/install/

Docker Compose
    https://github.com/docker/compose


## Installation

1. Clone the repository with: `git clone https://gitlab.gwdg.de/farbenlehre/gfl-app-new.git`
2. Copy `.env.dist` to `.env`. Add all required parameters to the `.env` file, such as the S3 and search index configuration. Ask the GFL team for datailed data aka. tokens and stuff.
3. Change the premission of solr folder with: `sudo chmod -R 777 solr`
4. Install backend dependencies with: `docker-compose up -d` and `docker-compose exec web composer install`.
5. Install frontend dependencies with `nvm use && npm install`.
6. Build and start local dev server: `npm run dev` or `npm run watch` (will instantly rebuild upon code changes).
7. Import, transform and index the TEI files with: `docker-compose exec web ./bin/console app:start_indexing [dev|prod]`

## Running locally

Start the application with `docker-compose up -d`.

Solr admin panel: 127.0.0.1:9990
GFL App instance: 127.0.0.1:1745

## Deployments

The application is deployed with Gitlab CI to the defined servers.
Each push to the Gitlab repository triggers the build of a docker container, containing the complete
Symfony App.
Deployments to the staging server is done by tagging a commit and doing a release
with this procedure. Tagging a commit also starts building a Docker image, that after its successful build
is deployed to the staging instances of the service.

The deployment of the live instance is done manually with Gitlab CI. When a commit is tagged, there is a button
to deploy the current live image to the live instance.

Starting the application on the server is done with the small shell script [restart.sh](https://gitlab.gwdg.de/digizeit/digizeit-app/snippets/114).

## Content Management 
Since we don't provide a visual interface to edit content. We implmented some workarounds to quickly perform 
textual edits. There are several files that hold text contents. Changes will be visible on the website after a
new deployment with the updated files.

- [Page texts and application strings](./translations/messages.de.yaml)
- [Monograph (Leittexte)](./assets/content/monograph.json) 
- [Imprint](./assets/content/imprint.md)
- [Privacy](./assets/content/privacy.md)
- [Image archives, image licenses, document languages, base URLs](./config/services.yaml)

## Contribution

* Write [good commit messages](http://chris.beams.io/posts/git-commit/)!
* Include an issue id in your commit message.
* Run ```./vendor/bin/php-cs-fixer fix``` prior to committing.
* Run ```phpunit``` to ensure all tests (including your new tests) are passing.

